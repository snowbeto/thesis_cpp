#ifndef CAMERA_PARAMETERS_HPP
#define CAMERA_PARAMETERS_HPP

#include <vector>
#include <configuration.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class CameraParameters {
public:
	static CameraParameters* Instance() {
		if (!instance) {
			instance = new CameraParameters();
		}
		return instance;
	}

	static void Delete() {
		if (instance) {
			cout << "Freeing memory" << endl;
			delete instance;
			instance = NULL;
		}

	}

	Mat get_M1() {
		return M1;
	}
	Mat get_M2() {
		return M2;
	}

	Mat getD1() {
		return D1;
	}

	Mat getD2() {
		return D2;
	}

	Mat getF() {
		return F;
	}

	Mat getH() {
		return H;
	}

	Size getImgSize() {
		return img_size;
	}

	/*static const CameraParameters*& getInstance() const {
	 return instance;
	 }*/

	Mat getM1() {
		return M1;
	}

	Mat getM2() {
		return M2;
	}

	Mat getMap11() {
		return map11;
	}

	Mat getMap12() {
		return map12;
	}

	Mat getMap21() {
		return map21;
	}

	Mat getMap22() {
		return map22;
	}

	Mat getMap11_lp() {
		return map11_lp;
	}

	Mat getMap12_lp() {
		return map12_lp;
	}

	Mat getMap21_lp() {
		return map21_lp;
	}

	Mat getMap22_lp() {
		return map22_lp;
	}

	Mat getP1() {
		return P1;
	}

	Mat getP2() {
		return P2;
	}

	Mat getQ() {
		return Q;
	}

	Mat getR() {
		return R;
	}

	Mat getR1() {
		return R1;
	}

	Mat getR2() {
		return R2;
	}

	Rect getRoi1() {
		return roi1;
	}

	Rect getRoi2() {
		return roi2;
	}

	Rect getRoi1_lp() {
		return roi1_lp;
	}

	Rect getRoi2_lp() {
		return roi2_lp;
	}

	Mat getT() {
		return T;
	}

	Size get_resize_size() {
		return resize_size;
	}

	Mat getF_lp() {
		return F_lp;
	}

	Mat getH_lp() {
		return H_lp;
	}

	Size getImgSize_lp() {
		return img_size_lp;
	}

	Mat getQ_lp() {
		return Q_lp;
	}

private:

	/*parameters for real camera*/
	std::string calibration_files_path;
	std::string intrinsic_filename;
	std::string extrinsic_filename;
	std::string homography_filename;
	Size img_size;
	Mat M1, D1, M2, D2;
	Mat R, T, R1, P1, R2, P2, Q, H, F;
	Mat map11, map12, map21, map22;
	Rect roi1, roi2;

	/*parameters for livepreview*/
	std::string calibration_files_path_lp;
	std::string intrinsic_filename_lp;
	std::string extrinsic_filename_lp;
	std::string homography_filename_lp;
	Size img_size_lp;
	Mat M1_lp, D1_lp, M2_lp, D2_lp;
	Mat R_lp, T_lp, R1_lp, P1_lp, R2_lp, P2_lp, Q_lp, H_lp, F_lp;
	Mat map11_lp, map12_lp, map21_lp, map22_lp;
	Rect roi1_lp, roi2_lp;

	Size resize_size;

	CameraParameters();

	CameraParameters(CameraParameters const& config) {
	}
	;

	CameraParameters& operator=(CameraParameters&) {
	}
	;

	static CameraParameters* instance;
};

#endif
