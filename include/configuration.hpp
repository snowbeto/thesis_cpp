#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP

#include <string>
#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

using namespace std;
using namespace cv;

class Configuration {
public:

	static Configuration* Instance() {
		if (!instance) {
			instance = new Configuration();
		}
		return instance;
	}

	static void Delete() {
		if (instance) {
			cout << "Freeing memory" << endl;
			delete instance;
			instance = NULL;
		}

	}

	void save_angles(string dir, string file_name,
			vector<vector<float>>& angles_complete);

	void update_frame_counter_file(int& val);
	void update_vid_counter_file(int& val);

	string get_left_camera_address() {
		return left_camera_address;
	}

	string get_right_camera_address() {
		return right_camera_address;
	}

	string get_camera_port() {
		return camera_port;
	}

	int get_minimum_valid_detected_marker_radius() {
		return minimum_valid_detected_marker_radius;
	}

	int get_calibration_tolerance() {
		return calibration_tolerance;
	}

	int get_min_euclidean_dist_between_markers() {
		return min_euclidean_dist_between_markers;
	}

	int get_block_size() {
		return block_size;
	}

	int get_number_of_disparities() {
		return number_of_disparities;
	}

	int get_wls_filter_lambda() {
		return wls_filter_lambda;
	}

	float get_wls_filter_sigmaColor() {
		return wls_filter_sigmaColor;
	}

	int get_viz_sphere_radius() {
		return viz_sphere_radius;
	}

	int get_viz_line_width() {
		return viz_line_width;
	}

	int get_viz_text_size() {
		return viz_text_size;
	}

	float get_max_depth() {
		return max_depth;
	}
	float get_reduction_ratio() {
		return reduction_ratio;
	}

	string get_left_video() {
		return left_video;
	}

	string get_right_video() {
		return right_video;
	}

	string get_video_dir() {
		return video_dir;
	}

	bool get_vertical_configuration() {
		return vertical_configuration;
	}

	int get_fps() {
		return fps;
	}

	bool get_use_real_cameras() {
		return use_real_cameras;
	}

	int get_frame_counter() {
		return frame_counter;
	}

	int get_vid_counter() {
		return vid_counter;
	}

	void save_ranges(vector<Scalar> &ranges, vector<Scalar> &lower_ranges);

	vector<Scalar> get_upper_ranges() {
		return upper_ranges;
	}

	vector<Scalar> get_lower_ranges() {
		return lower_ranges;
	}

	bool get_calibrate_maker_color() {
		return calibrate_maker_color;
	}

	string get_vid_dirs_file() {
		return vid_dirs_file;
	}

	string get_vid_list_file() {
		return vid_list_file;
	}

	string get_tranning_vids_dir() {
		return tranning_vids_dir;
	}
	string get_angles_file_extension() {
		return angles_file_extension;
	}

	string get_angles_variable_name() {
		return angles_variable_name;
	}

	string get_angles_size_variable_name() {
		return angles_size_variable_name;
	}

	bool get_store_angles() {
		return store_angles;
	}

	string get_svn_train_dir() {
		return svn_trainnig_dir;
	}
	string get_svn_predict_dir() {
		return svn_predict_dir;
	}
	string get_good_angles_file() {
		return good_angles_file;
	}
	string get_bad_angles_file() {
		return bad_angles_file;
	}

	bool get_generate_demo_videos() {
		return generate_demo_videos;
	}

	bool get_show_output() {
		return show_output;
	}

private:
	string config_dir;
	string config_file;
	string ranges_file;
	string counter_filemam;
	string video_counter_filename;
	string left_camera_address;
	string right_camera_address;
	string camera_port;
	int minimum_valid_detected_marker_radius;
	int calibration_tolerance;
	int min_euclidean_dist_between_markers;
	int block_size;
	int number_of_disparities;
	int wls_filter_lambda;
	float wls_filter_sigmaColor;
	int viz_sphere_radius;
	int viz_line_width;
	int viz_text_size;
	float max_depth;
	float reduction_ratio;
	string left_video;
	string right_video;
	string video_dir;
	bool vertical_configuration;
	int fps;
	bool use_real_cameras;
	int frame_counter;
	int vid_counter;
	vector<Scalar> upper_ranges, lower_ranges;
	string upper_scalar_string;
	string lower_scalar_string;
	string num_ranges_string;
	bool calibrate_maker_color;
	string vid_dirs_file;
	string vid_list_file;
	string tranning_vids_dir;
	string angles_file_extension;
	string angles_variable_name;
	string angles_size_variable_name;
	bool store_angles;
	string svn_trainnig_dir;
	string svn_predict_dir;
	string good_angles_file;
	string bad_angles_file;
	bool generate_demo_videos;
	bool show_output;

	Configuration();

	Configuration(Configuration const& config) {
	}
	;

	Configuration& operator=(Configuration&) {
	}
	;

	static Configuration* instance;

};

#endif
