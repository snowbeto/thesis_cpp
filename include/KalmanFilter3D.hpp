/*
 *  kalman filter.cpp
 *  calibration
 */

#ifndef KALMAN_FILTER_3D_HPP
#define KALMAN_FILTER_3D_HPP
#include "opencv2/video/tracking.hpp"
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;

class KalmanFilter3D {

public:
	KalmanFilter3D() : //initial x, y, z
			KF(9, 3, 0), measurement(3, 1) {

		float v = 1/1.4;
		float a = 0.5 * pow(v, 2);

		KF.transitionMatrix =
				(Mat_<float>(9, 9) << 1, 0, 0, v, 0, 0, a, 0, 0, 0, 1, 0, 0, v, 0, 0, a, 0, 0, 0, 1, 0, 0, v, 0, 0, a, 0, 0, 0, 1, 0, 0, v, 0, 0, 0, 0, 0, 0, 1, 0, 0, v, 0, 0, 0, 0, 0, 0, 1, 0, 0, v, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);

		measurement.setTo(Scalar(0));

		setIdentity(KF.measurementMatrix);
		setIdentity(KF.processNoiseCov, Scalar::all(1e-4));
		setIdentity(KF.measurementNoiseCov, Scalar::all(10));
		setIdentity(KF.errorCovPost, Scalar::all(.1));

	}

	void init(Point3f initial_position) {
		KF.statePre.at<float>(0) = initial_position.x;
		KF.statePre.at<float>(1) = initial_position.y;
		KF.statePre.at<float>(2) = initial_position.z;
		KF.statePre.at<float>(3) = 1;
		KF.statePre.at<float>(4) = 1;
		KF.statePre.at<float>(5) = 1;
		KF.statePre.at<float>(6) = 0.5;
		KF.statePre.at<float>(7) = 0.5;
		KF.statePre.at<float>(8) = 0.5;

		/*KF.measurementMatrix.at<float>(0, 0) = initial_position.x;
		 KF.measurementMatrix.at<float>(1, 1) = initial_position.y;
		 KF.measurementMatrix.at<float>(2, 2) = initial_position.z;
		 */

		estimate(initial_position);
		estimate(initial_position);
		estimate(initial_position);
		estimate(initial_position);
		estimate(initial_position);
		estimate(initial_position);
		estimate(initial_position);
		estimate(initial_position);
		estimate(initial_position);
		estimate(initial_position);
		estimate(initial_position);
		estimate(initial_position);
	}

	Point3f estimate(Point3f meassurments) {

		// First predict, to update the internal statePre variable
		Mat prediction = KF.predict();

		//Point3f predictPt(prediction.at<float>(0), prediction.at<float>(1),
		//		prediction.at<float>(2));

		measurement(0) = meassurments.x;
		measurement(1) = meassurments.y;
		measurement(2) = meassurments.z;

		//std::cout << "Predicted: " << predictPt << endl;
		// The update phase
		Mat estimated = KF.correct(measurement);

		return Point3f(estimated.at<float>(0), estimated.at<float>(1),
				estimated.at<float>(2));

	}

	Point3f predict() {
		Mat predictedPoint = KF.predict();
		return Point3f(predictedPoint.at<float>(0), predictedPoint.at<float>(1),
				predictedPoint.at<float>(2));
	}
private:
	KalmanFilter KF;
	Mat_<float> measurement;

};

#endif
