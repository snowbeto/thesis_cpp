#ifndef VIDEO_ANALYZER_HPP
#define VIDEO_ANALYZER_HPP
#include <opencv2/opencv.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/objdetect.hpp>
#include <string>
#include <opencv2/dpm.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/videoio/videoio_c.h>

using namespace cv;
using namespace std;
using namespace cv::dpm;
/*Code based on openCV implementation*/
class HumanDetectorAdapter: public cv::DetectionBasedTracker::IDetector {
public:
	HumanDetectorAdapter(cv::Ptr<cv::CascadeClassifier> detector) :
			IDetector(), a_detector(detector) {
		CV_Assert(detector);
	}
	;

	void detect(const cv::Mat &Image, std::vector<cv::Rect> &objects) {
		a_detector->detectMultiScale(Image, objects, scaleFactor, minNeighbours,
				0, minObjSize, maxObjSize);
	}
	;

	virtual ~HumanDetectorAdapter() {

	}
	;

private:
	HumanDetectorAdapter();
	cv::Ptr<cv::CascadeClassifier> a_detector;
};

class HumanDetector {
private:
	/*Code based on openCsV example*/
	std::string cascade_filename;
	cv::Ptr<cv::CascadeClassifier> cascade_main_detector;
	cv::Ptr<cv::CascadeClassifier> cascade_tracking_detector;
	cv::Ptr<cv::DetectionBasedTracker::IDetector> main_detector;
	cv::Ptr<cv::DetectionBasedTracker::IDetector> tracking_detector;
	cv::DetectionBasedTracker::Parameters parameters;
	cv::DetectionBasedTracker human_detector;
public:
	HumanDetector() :
			cascade_filename(
					"./data/body_haar_cascade_templates/haarcascade_lowerbody.xml"), cascade_main_detector(
					cv::makePtr < cv::CascadeClassifier > (cascade_filename)), main_detector(
					cv::makePtr < HumanDetectorAdapter
							> (cascade_main_detector)), cascade_tracking_detector(
					cv::makePtr < cv::CascadeClassifier > (cascade_filename)), tracking_detector(
					cv::makePtr < HumanDetectorAdapter
							> (cascade_tracking_detector)), human_detector(
					main_detector, tracking_detector, parameters) {

	}
	;

	void detect_loweboddy(cv::Mat& frame, cv::Mat& grayFrame) {
		std::cout << "Detecting human ... " << std::endl;
		std::vector<cv::Rect> body_candidates;
		/*human_detector.process(grayFrame);
		 human_detector.getObjects(body_candidates);
		 std::cout << "Number of Candidates " << body_candidates.size() << endl;
		 for (size_t i = 0; i < body_candidates.size(); i++) {
		 cv::rectangle(frame, body_candidates[i], cv::Scalar(0, 255, 0)); /*Parameters taken from openCV example
		 }*/

		string filename("./data/inriaperson.xml");
		cv::Ptr<DPMDetector> detector = DPMDetector::create(
				vector<string>(1, filename));
		vector<DPMDetector::ObjectDetection> ds;
		detector->detect(frame, ds);
		drawBoxes(frame, ds, Scalar(0, 255, 255), "Human");

	}

	void drawBoxes(Mat &frame, vector<DPMDetector::ObjectDetection> ds,
			Scalar color, string text) {
		for (unsigned int i = 0; i < ds.size(); i++) {
			rectangle(frame, ds[i].rect, color, 2);
		}

		// draw text on image
		Scalar textColor(0, 0, 250);
		/*putText(frame, text, Point(10, 50), FONT_HERSHEY_PLAIN, 2, textColor,
				2);*/
	}

};
#endif
