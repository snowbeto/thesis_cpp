#ifndef ANALYZER_HPP
#define ANALYZER_HPP

#include <vector>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>
#include <KalmanFilter3D.hpp>

using namespace std;
using namespace cv;

class Analyzer {
public:
	void separate_points(vector<Point3f> & points,
			vector<Point3f> &skeleton_points, bool& valid,
			vector<int>& counters);
	void compute_angles(vector<Point3f> &points, vector<float> &angles);
	void separate_points_bottom_top(vector<Point3f> & points,
			vector<Point3f> &skeleton_points);
	void separate_points_right_left(vector<Point3f> & points,
			vector<Point3f> &skeleton_points);
	Analyzer() {
		aFlag = false;
	}
	;

private:
	bool aFlag;
	vector<Point3f> previous_state;
	KalmanFilter3D KFmarkers[7];

};

#endif
