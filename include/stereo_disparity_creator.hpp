/*
 *  stereo_match.cpp
 *  calibration
 *
 *  Created by Victor  Eruhimov on 1/18/10.
 *  Copyright 2010 Argus Corp. All rights reserved.
 *
 */
#ifndef STEREO_DISPARITY_CREATOR   // if x.h hasn't been included yet...
#define STEREO_DISPARITY_CREATOR

#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/ximgproc/disparity_filter.hpp"
#include <configuration.hpp>
#include <camera_parameters.hpp>

#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;
using namespace cv;
using namespace cv::ximgproc;

class StereoDisparityCreator {
public:

	Mat create_pcl_file(const Mat& filtered_disp_vis);
	enum {
		STEREO_3WAY = 4
	};
	StereoDisparityCreator();

	Mat get_disparity_map(Mat left, Mat right);
	Mat get_disparity_map_bm(Mat left, Mat right);
	vector<Point3f> get_point_depth(const Mat& filtered_disp_vis, Mat &image,
			vector<Point> centers);
	void undistort_left_image(Mat& mat);
	void undistrort_right_image(Mat& mat);

	void undistort_left_image_lp(Mat& mat);
	void undistrort_right_image_lp(Mat& mat);

	void match_right__to_left(Mat & mat);
	void match_points_rigth_image_to_left_image(vector<Point2f> &points_right,
			vector<Point2f> &points_left);

	bool is_valid_point(const Mat& filtered_disp_vis, Point2f marker);
	void compute_3d_image(Mat & disp_map);

private:
	enum {
		left_image_index = 1, right_image_index = 2
	};
	float max_depth;
	float reduction_ratio;
	std::string disparity_filename;
	std::string point_cloud_filename;
	Ptr<DisparityWLSFilter> wls_filter;
	Ptr<DisparityWLSFilter> wls_filter_bm;
	int alg;
	int SADWindowSize; // The block size (--blocksize=<...>) must be a positive odd number
	int numberOfDisparities; //The max disparity (--maxdisparity=<...>) must be a positive integer divisible by 16.
	Mat disparity_map;
	Mat point_cloud;
	Ptr<StereoSGBM> left_sgbm;
	Ptr<StereoBM> left_bm;
	Rect roi1, roi2;
	Mat Q;
	Mat H;
	Mat F;
	Size img_size;
	Mat map11, map12, map21, map22;
	Mat _3d_image;
	//Configuration* config;
};
#endif
