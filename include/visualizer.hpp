#ifndef VISUALIZER_
#define VISUALIZER_

#include <opencv2/viz/vizcore.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>

#include <iostream>
#include <cmath>
#include <string>
#include <configuration.hpp>

using namespace cv;
using namespace std;

/*
 * Hacer una clase que reciva aun array de 3 puntos por linea
 * regresar un arral con los ángulos
 * agregar un manejador para las lines
 *
 * */

class Visualizer {
public:

	bool wasStopped();
	void spinOnce();
	void close();

	Visualizer();

	void removeAllWidgets();
	/*void add_elemnets(const vector<Point3f> &skeleton_points,
	 const vector<float> &angles, const vector<Point3f> &right_points,
	 const vector<float> &right_angles);*/
	void add_elemnets(const vector<Point3f> &points,
			const vector<float> angles);

	float get_distance(const Point3f& point1, const Point3f& point2) {
		return sqrt(
				pow(point1.x - point2.x, 2.0) + pow(point1.y - point2.y, 2.0)
						+ pow(point1.z - point2.z, 2.0));
	}
	;
	Mat get_frame() {
		return myWindow.getScreenshot();
	}
	;

	void reset_camera() {
		myWindow.resetCamera();
	}
private:

	int line_width;
	int spehre_radius;
	int text_size;
	int sphere_resolution;
	viz::Viz3d myWindow;
};

/**
 * @function main
 */

#endif
