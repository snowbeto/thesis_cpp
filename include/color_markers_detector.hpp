#ifndef COLOR_MARKERS_DETECTORS
#define COLOR_MARKERS_DETECTORS

#include <opencv2/opencv.hpp>
#include <cmath>
#include <string>
#include <iostream>
#include <utility>
#include <stereo_disparity_creator.hpp>
#include <configuration.hpp>

using namespace cv;
using namespace std;

class Predicate {
public:
	int _dist2;
	Predicate(int dist) :
			_dist2(dist * dist) {
	}

	bool operator()(const Point& lhs, const Point& rhs) const {
		return ((lhs.x - rhs.x) * (lhs.x - rhs.x)
				+ (lhs.y - rhs.y) * (lhs.y - rhs.y)) < _dist2;
	}
};

class ColorMarkersDetctor {
public:
	/*Code copied from the Internet*/
	Mat normalize_image(Mat bgr_image);
	Mat normalize_image_rgb(Mat image);
	static void mouseCallback(int event, int x, int y, int flags, void *param);
	void callbackFunc(int event, int x, int y, int flags);
	bool calibrate(Mat frame, Mat right);
	vector<Point2f> detect_markers_moments(Mat &frame, Mat &mask);
	vector<Point> detect_markers_clustering(Mat &frame, Mat &mask);
	vector<Point> detect_markers_combined(Mat & left, Mat &right_converted,
			Mat & mask_a, Mat & mask_b, Mat &mask_c, Mat& depth_image);
	ColorMarkersDetctor(string ranges_dir);
	void compute_from_videocap(int camera_id);
	void compute_centers(Mat& ref, Mat& result, vector<Point2f> &centers,
			vector<float> &radius_vec, Mat& disp);
	void get_lower_upper_ranges();
	void init_lower_upper_ranges();
	void read_ranges() {
		//Read stored calibration ranges
		FileStorage fs2(ranges_dir + ranges_file, FileStorage::READ);

		Scalar scalar;
		int nRanges;
		fs2[num_ranges_string] >> nRanges;
		for (int i = 0; i < nRanges; ++i) {

			/*cout << upper_scalar_string + to_string(i) << endl;
			 cout << lower_scalar_string + to_string(i) << endl;*/

			fs2[upper_scalar_string + to_string(i)] >> scalar;
			upper_ranges.push_back(scalar);
			fs2[lower_scalar_string + to_string(i)] >> scalar;
			lower_ranges.push_back(scalar);
		}
		fs2.release();
	}

	void save_ranges(vector<Scalar> &upper_ranges,
			vector<Scalar> &lower_ranges) {
		if (upper_ranges.size() < 1)
			return;

		FileStorage fs(ranges_dir + ranges_file, FileStorage::WRITE);

		for (int i = 0; i < upper_ranges.size(); i++) {
			fs << upper_scalar_string + to_string(i) << upper_ranges[i];
			fs << lower_scalar_string + to_string(i) << lower_ranges[i];
		}

		int size = upper_ranges.size();
		fs << num_ranges_string << size;

		fs.release();
	}

	int get_tolerance() {
		return tolerance;
	}
	void set_tolerance(int tolerance) {
		this->tolerance = tolerance;
	}
private:
	Mat combined_frame;
	Mat calib_frame;
	bool calibration_done;
	bool drag;
	Point point1;
	Point point2;
	int radius;
	bool click_one;
	bool click_two;
	cv::Scalar lower_range, upper_range;
	int tolerance;
	Rect roi;
	vector<pair<Point, Point> > calibraition_points;
	//vector<pair<Scalar, Scalar> > ranges;
	vector<Scalar> upper_ranges, lower_ranges;
	int color_space;
	StereoDisparityCreator stereoMatcher;
	int euclidean_distance;
	string ranges_dir;
	string upper_scalar_string, lower_scalar_string, num_ranges_string,
			ranges_file;

	//vctor<Point> previous_location;
	//Configuration *config;

};

#endif
