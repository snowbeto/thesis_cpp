% Auto-generated by stereoCalibrator app on 24-May-2016
%-------------------------------------------------------


% Define images to process
imageFileNames1 = {'/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left01.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left02.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left03.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left04.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left05.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left06.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left07.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left08.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left09.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left10.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left11.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left12.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left13.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left14.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left15.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left16.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left17.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left18.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left19.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left20.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left21.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left22.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left23.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left24.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left25.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left26.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left27.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left28.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/left/left29.jpg',...
    };
imageFileNames2 = {'/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right01.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right02.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right03.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right04.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right05.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right06.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right07.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right08.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right09.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right10.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right11.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right12.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right13.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right14.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right15.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right16.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right17.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right18.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right19.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right20.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right21.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right22.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right23.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right24.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right25.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right26.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right27.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right28.jpg',...
    '/home/daniel/Dropbox/hbrs/semester4/thesis/cameraAPI/curlpp/bin/data/calib_images/right/right29.jpg',...
    };

% Detect checkerboards in images
[imagePoints, boardSize, imagesUsed] = detectCheckerboardPoints(imageFileNames1, imageFileNames2);

% Generate world coordinates of the checkerboard keypoints
squareSize = 72;  % in units of 'mm'
worldPoints = generateCheckerboardPoints(boardSize, squareSize);

% Calibrate the camera
[stereoParams, pairsUsed, estimationErrors] = estimateCameraParameters(imagePoints, worldPoints, ...
    'EstimateSkew', false, 'EstimateTangentialDistortion', false, ...
    'NumRadialDistortionCoefficients', 2, 'WorldUnits', 'mm', ...
    'InitialIntrinsicMatrix', [], 'InitialRadialDistortion', []);

% View reprojection errors
h1=figure; showReprojectionErrors(stereoParams, 'CameraCentric');

% Visualize pattern locations
h2=figure; showExtrinsics(stereoParams, 'CameraCentric');

% Display parameter estimation errors
displayErrors(estimationErrors, stereoParams);

% You can use the calibration data to rectify stereo images.
I1 = imread(imageFileNames1{1});
I2 = imread(imageFileNames2{1});
[J1, J2] = rectifyStereoImages(I1, I2, stereoParams);

% See additional examples of how to use the calibration data.  At the prompt type:
% showdemo('StereoCalibrationAndSceneReconstructionExample')
% showdemo('DepthEstimationFromStereoVideoExample')
