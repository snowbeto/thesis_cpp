/*
 *  stereo_match.cpp
 *  calibration
 *
 *  Created by Victor  Eruhimov on 1/18/10.
 *  Copyright 2010 Argus Corp. All rights reserved.
 *
 */
#include <stereo_disparity_creator.hpp>
#include <iostream>

Mat StereoDisparityCreator::create_pcl_file(const Mat& filtered_disp_vis) {
	Mat mat;
	reprojectImageTo3D(filtered_disp_vis, mat, Q, true);
	const double max_z = 1.0e4;
	std::vector<Vec3f> points;
	for (int y = 0; y < mat.rows; y++) {
		for (int x = 0; x < mat.cols; x++) {
			Vec3f point = mat.at<Vec3f>(y, x);
			if (fabs(point[2] - max_z) < FLT_EPSILON || fabs(point[2]) > max_z)
				continue;
			points.push_back(point);
		}

	}

	FILE* fp = fopen(point_cloud_filename.c_str(), "wt");
	fprintf(fp, "VERSION .7\n");
	fprintf(fp, "FIELDS x y z\n");
	fprintf(fp, "SIZE 4 4 4\n");
	fprintf(fp, "TYPE F F F\n");
	fprintf(fp, "COUNT 1 1 1\n");
	fprintf(fp, "WIDTH %d\n", (int) points.size());
	fprintf(fp, "HEIGHT 1\n");
	fprintf(fp, "VIEWPOINT 0 0 0 1 0 0 0\n");
	fprintf(fp, "POINTS  %d\n", (int) points.size());
	fprintf(fp, "DATA ascii\n");

	for (int y = 0; y < points.size(); y++) {
		Vec3f point = points[y];
		fprintf(fp, "%f %f %f\n", point[0], point[1], point[2]);
	}
	fclose(fp);
}

vector<Point3f> StereoDisparityCreator::get_point_depth(
		const Mat& filtered_disp_vis, Mat &image, vector<Point> centers) {
	Mat mat;
	Vec3f point;
	vector<Point3f> points;
	reprojectImageTo3D(filtered_disp_vis, mat, Q, false);

	for (int x = 0; x < centers.size(); x++) {

		Point center = centers[x];
		int start_row, end_row;
		int start_col, end_col;
		int number_of_neigbors = 3;

		start_col = center.x - number_of_neigbors;
		if (start_col < 0)
			start_col = 0;
		end_col = center.x + number_of_neigbors;
		if (end_col > mat.cols)
			end_col = mat.cols - 1;

		start_row = center.y - number_of_neigbors;
		if (start_row < 0)
			start_row = 0;
		end_row = center.y + number_of_neigbors;
		if (end_row > mat.rows)
			end_row = mat.rows - 1;

		int counter = 0;
		Vec3f depth_point(0.0, 0.0, 0.0);
		for (int x = start_col; x < end_col; x++) {
			for (int y = start_row; y < end_row; y++) {
				point = mat.at<Vec3f>(Point(x, y));
				depth_point = depth_point + point;
				counter++;
			}
		}

		depth_point = depth_point / counter;

		if (point[2] < max_depth) {
			point[1] = point[1] * -1;
			point[2] = point[2] * -1;

			points.push_back(point);

			//cout << "Point: " << point << endl;
			//cout << "Depth point: " << depth_point << endl;
			std::string text = /*"x: " + to_string(point[0]) + " y: "
			 + to_string(point[1]) + */" z:" + to_string(point[2]);
			std::string text2 = /*"x: " + to_string(depth_point[0]) + " y: "
			 + to_string(depth_point[1]) + */" z:" + to_string(depth_point[2]);
			putText(image, text, centers[x], FONT_HERSHEY_COMPLEX, 0.5,
					Scalar::all(255), 1, 1);

			putText(filtered_disp_vis, text, centers[x], FONT_HERSHEY_COMPLEX,
					0.5, Scalar::all(255), 1, 1);
			/*putText(filtered_disp_vis, to_string(point[2]), centers[x],
			 FONT_HERSHEY_SCRIPT_SIMPLEX, 1, Scalar::all(255), 1, 1);*/
		}

	}

	return points;
}
;

bool StereoDisparityCreator::is_valid_point(const Mat& filtered_disp_vis,
		Point2f marker) {

	Vec3f point = _3d_image.at<Vec3f>(Point(marker.x, marker.y));

	bool result = false;

	if (point[2] < max_depth)
		result = true;

	//cout << "Valid point (" << point[2] << " < " << max_depth << ") = "
	//		<< result << endl;

	return result;
}
;
enum {
	STEREO_3WAY = 4
};

StereoDisparityCreator::StereoDisparityCreator() :
		disparity_filename("disparity_image.jpg"), point_cloud_filename(
				"./my_pcl.pcd"), alg(STEREO_3WAY), SADWindowSize(
				Configuration::Instance()->get_block_size()), numberOfDisparities(
				Configuration::Instance()->get_number_of_disparities()), left_sgbm(
				StereoSGBM::create(0, 16, 3)), left_bm(
				StereoBM::create(numberOfDisparities, SADWindowSize)), roi1(
				CameraParameters::Instance()->getRoi1()), roi2(
				CameraParameters::Instance()->getRoi2()), Q(
				CameraParameters::Instance()->getQ()), H(
				CameraParameters::Instance()->getH()), F(
				CameraParameters::Instance()->getF()), img_size(
				CameraParameters::Instance()->getImgSize()), map11(
				CameraParameters::Instance()->getMap11()), map12(
				CameraParameters::Instance()->getMap12()), map21(
				CameraParameters::Instance()->getMap21()), map22(
				CameraParameters::Instance()->Instance()->getMap22()), max_depth(
				Configuration::Instance()->get_max_depth()), reduction_ratio(
				Configuration::Instance()->get_reduction_ratio()) {

}
;

void StereoDisparityCreator::undistort_left_image(Mat& mat) {
	resize(mat, mat, CameraParameters::Instance()->get_resize_size());
	Mat mat_u;
	Mat mask = Mat::zeros(mat.size(), mat.type());
	Mat r = Mat::zeros(mat.size(), mat.type());
	rectangle(mask, roi1, Scalar(255, 255, 255), -1);
	remap(mat, mat_u, map11, map12, INTER_LINEAR);
	mat_u.copyTo(r, mask);
	mat = r;

}
;

void StereoDisparityCreator::undistrort_right_image(Mat &mat) {
	resize(mat, mat, CameraParameters::Instance()->get_resize_size());
	Mat mat_u;
	Mat mask = Mat::zeros(mat.size(), mat.type());
	Mat r = Mat::zeros(mat.size(), mat.type());
	rectangle(mask, roi2, Scalar(255, 255, 255), -1);
	remap(mat, mat_u, map21, map22, INTER_LINEAR);
	mat_u.copyTo(r, mask);
	mat = r;
}
;

void StereoDisparityCreator::undistort_left_image_lp(Mat& mat) {
	Mat mat_u;
	Mat mask = Mat::zeros(mat.size(), mat.type());
	Mat r = Mat::zeros(mat.size(), mat.type());
	rectangle(mask, CameraParameters::Instance()->getRoi1_lp(),
			Scalar(255, 255, 255), -1);
	remap(mat, mat_u, CameraParameters::Instance()->getMap11_lp(),
			CameraParameters::Instance()->getMap12_lp(), INTER_LINEAR);
	mat_u.copyTo(r, mask);
	mat = r;

}
;

void StereoDisparityCreator::undistrort_right_image_lp(Mat &mat) {
	Mat mat_u;
	Mat mask = Mat::zeros(mat.size(), mat.type());
	Mat r = Mat::zeros(mat.size(), mat.type());
	rectangle(mask, CameraParameters::Instance()->getRoi2_lp(),
			Scalar(255, 255, 255), -1);
	remap(mat, mat_u, CameraParameters::Instance()->getMap21_lp(),
			CameraParameters::Instance()->getMap22_lp(), INTER_LINEAR);
	mat_u.copyTo(r, mask);
	mat = r;
}
;

void StereoDisparityCreator::match_points_rigth_image_to_left_image(
		vector<Point2f> &points_right, vector<Point2f> &points_left) {

	/*cout << " *********S********" << endl;
	 vector<Point3f> lines;
	 computeCorrespondEpilines(points_right, right_image_index, F, lines);
	 vector<Point3f> points_right_h;
	 convertPointsToHomogeneous(points_right, points_right_h);
	 for (int i = 0; i < points_right_h.size(); i++) {

	 Point3f point_right = points_right_h[i];
	 Point3f point_left = point_right;
	 float val = 100;
	 int point_x;
	 cout << "point " << i << point_right << endl;
	 for (int y = 0; y < img_size.width; y++) {
	 point_left.y = y;
	 Mat x(point_left);
	 Mat x_(point_right);
	 x.convertTo(x, CV_32F);
	 x_.convertTo(x_, CV_32F);
	 F.convertTo(F, CV_32F);

	 Mat result = x_.t() * F * x;

	 if (norm(result) < val) {
	 val = norm(result);
	 point_x = y;

	 }
	 }
	 Point2f point(point_right.x, point_x);
	 points_left.push_back(point);
	 cout << "Point r " << point << " result: " << val << endl;
	 }
	 cout << " *********E********" << endl;*/
	points_left = points_right;

}

void StereoDisparityCreator::match_right__to_left(Mat & mat) {
	warpPerspective(mat, mat, H.inv(), mat.size());
}

void StereoDisparityCreator::compute_3d_image(Mat &disp_map) {

	reprojectImageTo3D(disp_map, _3d_image, Q, false);

}
Mat StereoDisparityCreator::get_disparity_map(Mat left, Mat right) {
	Mat left_for_matcher, right_for_matcher;
	float t = getTickCount();
	if (left.empty()) {
		printf("Could not load the left input image file\n");
		return Mat();
	}
	if (right.empty()) {
		printf("Could not load the right input image file\n");
		return Mat();
	}

	/*Mat leftr, rightr;
	 remap(left, leftr, map11, map12, INTER_LINEAR);
	 remap(right, rightr, map21, map22, INTER_LINEAR);

	 left = leftr;
	 right = rightr;*/
	undistort_left_image(left);
	undistrort_right_image(right);

	left_for_matcher = left.clone();
	right_for_matcher = right.clone();
	Mat left_disp, right_disp;
	Mat filtered_disp;
	Mat conf_map = Mat(left.rows, left.cols, CV_8U);
	double vis_mult = 1.0;
	conf_map = Scalar(255);
	Rect ROI;
	numberOfDisparities =
			numberOfDisparities > 0 ?
					numberOfDisparities : ((img_size.width / 8) + 15) & -16;

	left_sgbm->setPreFilterCap(63);
	int sgbmWinSize = SADWindowSize > 0 ? SADWindowSize : 3;
	left_sgbm->setBlockSize(sgbmWinSize);

	int cn = left.channels();

	left_sgbm->setP1(8 * cn * sgbmWinSize * sgbmWinSize);
	left_sgbm->setP2(32 * cn * sgbmWinSize * sgbmWinSize);
	left_sgbm->setMinDisparity(0);
	left_sgbm->setNumDisparities(numberOfDisparities);
	left_sgbm->setUniquenessRatio(10);
	left_sgbm->setSpeckleWindowSize(100);
	left_sgbm->setSpeckleRange(32);
	left_sgbm->setDisp12MaxDiff(1);
	left_sgbm->setMode(StereoSGBM::MODE_SGBM_3WAY);

	wls_filter = createDisparityWLSFilter(left_sgbm);
	const Ptr<StereoMatcher> right_matcher = createRightMatcher(left_sgbm);

	cvtColor(left_for_matcher, left_for_matcher, COLOR_BGR2GRAY);
	cvtColor(right_for_matcher, right_for_matcher, COLOR_BGR2GRAY);

	left_sgbm->compute(left_for_matcher, right_for_matcher, left_disp);
	right_matcher->compute(right_for_matcher, left_for_matcher, right_disp);

	//t = getTickCount() - t;
	//printf("Time elapsed: %fms\n", t * 1000 / getTickFrequency());

	wls_filter->setLambda(Configuration::Instance()->get_wls_filter_lambda());
	wls_filter->setSigmaColor(
			Configuration::Instance()->get_wls_filter_sigmaColor());
	//t = getTickCount();
	wls_filter->filter(left_disp, left, filtered_disp, right_disp);
	//t = getTickCount() - t;
	//printf("Filtering time: %fms\n", t * 1000 / getTickFrequency());

	Mat filtered_disp_vis;
	getDisparityVis(filtered_disp, filtered_disp_vis, vis_mult);

	Mat mask = Mat::zeros(filtered_disp_vis.size(), filtered_disp_vis.type());
	Mat r = Mat::zeros(filtered_disp_vis.size(), filtered_disp_vis.type());

	//reduce roi width as there is distorsion on the sides which affect the disparity map
	Rect smaller_roi = roi1;
	int reduction = (smaller_roi.width * reduction_ratio);
	smaller_roi.x = smaller_roi.x + (reduction / 2);
	smaller_roi.width = smaller_roi.width - reduction;

	rectangle(mask, smaller_roi, Scalar(255, 255, 255), -1);
	filtered_disp_vis.copyTo(r, mask);
	filtered_disp_vis = r;

	return filtered_disp_vis;
}

Mat StereoDisparityCreator::get_disparity_map_bm(Mat left, Mat right) {
	Mat left_for_matcher, right_for_matcher;
	float t = getTickCount();
	if (left.empty()) {
		printf("Could not load the left input image file\n");
		return Mat();
	}
	if (right.empty()) {
		printf("Could not load the right input image file\n");
		return Mat();
	}

	/*Mat leftr, rightr;
	 remap(left, leftr, map11, map12, INTER_LINEAR);
	 remap(right, rightr, map21, map22, INTER_LINEAR);

	 left = leftr;
	 right = rightr;*/
	undistort_left_image(left);
	undistrort_right_image(right);

	left_for_matcher = left.clone();
	right_for_matcher = right.clone();
	Mat left_disp, right_disp;
	Mat filtered_disp;
	Mat conf_map = Mat(left.rows, left.cols, CV_8U);
	double vis_mult = 1.0;
	conf_map = Scalar(255);
	Rect ROI;

	wls_filter_bm = createDisparityWLSFilter(left_bm);
	const Ptr<StereoMatcher> right_matcher = createRightMatcher(left_bm);

	cvtColor(left_for_matcher, left_for_matcher, COLOR_BGR2GRAY);
	cvtColor(right_for_matcher, right_for_matcher, COLOR_BGR2GRAY);

	left_bm->compute(left_for_matcher, right_for_matcher, left_disp);
	right_matcher->compute(right_for_matcher, left_for_matcher, right_disp);

	t = getTickCount() - t;
	printf("Time elapsed: %fms\n", t * 1000 / getTickFrequency());

	wls_filter_bm->setLambda(8000);
	wls_filter_bm->setSigmaColor(1.5);
	t = getTickCount();
	wls_filter_bm->filter(left_disp, left, filtered_disp, right_disp);
	t = getTickCount() - t;
	printf("Filtering time: %fms\n", t * 1000 / getTickFrequency());

	Mat filtered_disp_vis;
	getDisparityVis(filtered_disp, filtered_disp_vis, vis_mult);

	Mat mat_u;
	Mat mask = Mat::zeros(filtered_disp_vis.size(), filtered_disp_vis.type());
	Mat r = Mat::zeros(filtered_disp_vis.size(), filtered_disp_vis.type());
	rectangle(mask, roi1, Scalar(255, 255, 255), -1);
	mat_u.copyTo(r, mask);
	filtered_disp_vis = r;

	return filtered_disp_vis;
}
