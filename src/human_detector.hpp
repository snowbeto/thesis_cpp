/*
 * human_detector.hpp
 *
 *  Created on: Nov 26, 2015
 *      Author: daniel
 */

#ifndef INCLUDE_HUMAN_DETECTOR_HPP_
#define INCLUDE_HUMAN_DETECTOR_HPP_

#include <opencv2/objdetect/objdetect.hpp>

/*Code based on openCV implementation*/
class HumanDetectorAdapter: public cv::DetectionBasedTracker::IDetector {
public:
	HumanDetectorAdapter(cv::Ptr<cv::CascadeClassifier> detector);

	void detect(const cv::Mat &frame, std::vector<cv::Rect> &found_objs);

	virtual ~HumanDetectorAdapter();

private:
	HumanDetectorAdapter();
	cv::Ptr<cv::CascadeClassifier> a_detector;
};

#endif /* INCLUDE_HUMAN_DETECTOR_HPP_ */
