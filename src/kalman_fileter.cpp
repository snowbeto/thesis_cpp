#include "opencv2/highgui/highgui.hpp"
#include "opencv2/video/tracking.hpp"
#include <iostream>

#define drawCross( center, color, d )                                 \
line( img, Point( center.x - d, center.y - d ), Point( center.x + d, center.y + d ), color, 2, CV_AA, 0); \
line( img, Point( center.x + d, center.y - d ), Point( center.x - d, center.y + d ), color, 2, CV_AA, 0 )

using namespace cv;
using namespace std;

Point mousePos;

void mouseCallback(int event, int x, int y, int flags, void *param) {
	mousePos.x = x;
	mousePos.y = y;
}

int main() {

//2d kalman filter KalmanFilter KF(4, 2, 0);
// intialization of KF...
	/*KF.transitionMatrix =
	 (Mat_<float>(4, 4) << 1, 0, 1, 0,
	 0, 1, 0, 1,
	 0, 0, 1, 0,
	 0, 0, 0, 1);/*
	 /*Mat_<float> measurement(2, 1);*/
	//measurement.setTo(Scalar(0));
	/*KF.statePre.at<float>(0) = mousePos.x;
	 KF.statePre.at<float>(1) = mousePos.y;
	 KF.statePre.at<float>(2) = 0;
	 KF.statePre.at<float>(3) = 0;*/

//3d Kalman Filter
	KalmanFilter KF(9, 3, 0);
	// intialization of KF...
	float v = 1 ;
	float a = 0.5 * pow(v, 2);

	KF.transitionMatrix =
			(Mat_<float>(9, 9) << 1, 0, 0, v, 0, 0, a, 0, 0, 0, 1, 0, 0, v, 0, 0, a, 0, 0, 0, 1, 0, 0, v, 0, 0, a, 0, 0, 0, 1, 0, 0, v, 0, 0, 0, 0, 0, 0, 1, 0, 0, v, 0, 0, 0, 0, 0, 0, 1, 0, 0, v, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1);
	Mat_<float> measurement(3, 1);
	measurement.setTo(Scalar(0));
	KF.statePre.at<float>(0) = mousePos.x;
	KF.statePre.at<float>(1) = mousePos.y;
	KF.statePre.at<float>(2) = 1;
	KF.statePre.at<float>(3) = 0;
	KF.statePre.at<float>(4) = 0;
	KF.statePre.at<float>(5) = 0;
	KF.statePre.at<float>(6) = 0;
	KF.statePre.at<float>(7) = 0;
	KF.statePre.at<float>(8) = 0;

	setIdentity(KF.measurementMatrix);
	setIdentity(KF.processNoiseCov, Scalar::all(1e-4));
	setIdentity(KF.measurementNoiseCov, Scalar::all(10));
	setIdentity(KF.errorCovPost, Scalar::all(.1));
// Image to show mouse tracking
	Mat img(600, 800, CV_8UC3);
	vector<Point> mousev, kalmanv;
	mousev.clear();
	kalmanv.clear();

	while (1) {
		// First predict, to update the internal statePre variable
		Mat prediction = KF.predict();
		Point3f predictPt(prediction.at<float>(0), prediction.at<float>(1),
				prediction.at<float>(2));

		int keyPressed = waitKey(1);

		if (keyPressed == 1048603) {
			break;
		}

		if (keyPressed == 1048681) { //i
			//save images
			measurement(0) = 0;
			measurement(1) = 0;
			measurement(2) = 1;
		}

		else {
			// Get mouse point
			setMouseCallback("mouse kalman", mouseCallback);
			measurement(0) = mousePos.x;
			measurement(1) = mousePos.y;
			measurement(2) = 1;
		}

		std::cout << "Predicted: " << predictPt << endl;
		// The update phase
		Mat estimated = KF.correct(measurement);

		Point statePt(estimated.at<float>(0), estimated.at<float>(1));
		Point measPt(measurement(0), measurement(1));
		// plot points
		imshow("mouse kalman", img);
		img = Scalar::all(0);

		mousev.push_back(measPt);
		kalmanv.push_back(statePt);
		drawCross(statePt, Scalar(255, 255, 255), 5);
		drawCross(measPt, Scalar(0, 0, 255), 5);

		for (int i = 0; i < mousev.size() - 1; i++)
			line(img, mousev[i], mousev[i + 1], Scalar(255, 0, 0), 1);

		for (int i = 0; i < kalmanv.size() - 1; i++)
			line(img, kalmanv[i], kalmanv[i + 1], Scalar(0, 255, 0), 1);

	}

	return 0;
}
