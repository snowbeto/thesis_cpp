#include<color_markers_detector.hpp>

/*Code copied from the internet*/
Mat ColorMarkersDetctor::normalize_image(Mat bgr_image) {
	// READ RGB color image and convert it to Lab
	/*cv::Mat lab_image;
	 cv::cvtColor(bgr_image, lab_image, CV_BGR2Lab);

	 // Extract the L channel
	 std::vector<cv::Mat> lab_planes(3);
	 cv::split(lab_image, lab_planes); // now we have the L image in lab_planes[0]

	 // apply the CLAHE algorithm to the L channel
	 cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
	 clahe->setClipLimit(2);
	 cv::Mat dst;
	 clahe->apply(lab_planes[0], dst);

	 // Merge the the color planes back into an Lab image
	 dst.copyTo(lab_planes[0]);
	 cv::merge(lab_planes, lab_image);

	 // convert back to RGB
	 cv::Mat image_clahe;
	 cv::cvtColor(lab_image, image_clahe, CV_Lab2BGR);*/

	GaussianBlur(bgr_image, bgr_image, Size(3, 3), 0, 0);

	return bgr_image;
}

Mat ColorMarkersDetctor::normalize_image_rgb(Mat image) {
	Mat dst;
	cvtColor(image, image, CV_BGR2GRAY);
	equalizeHist(image, dst);
	cvtColor(dst, image, CV_GRAY2BGR);
	return image;

}
void ColorMarkersDetctor::mouseCallback(int event, int x, int y, int flags,
		void *param) {
	ColorMarkersDetctor *self = static_cast<ColorMarkersDetctor*>(param);
	self->callbackFunc(event, x, y, flags);
}
void ColorMarkersDetctor::callbackFunc(int event, int x, int y, int flags) {

	if (event == CV_EVENT_LBUTTONDOWN && !drag) {
		point1.x = x;
		point1.y = y;
		drag = true;
	}

	if (event == CV_EVENT_MOUSEMOVE && drag) {

		point2.x = x;
		point2.y = y;
		rectangle(calib_frame, point1, point2, Scalar(255, 25, 190));

	}

	if (event == CV_EVENT_LBUTTONUP && drag) {
		point2 = Point(x, y);
		roi = Rect(point1, point2);
		calibration_done = true;
		drag = false;
		cout << "Point1: " << point1 << " point2: " << point2 << endl;
		calibraition_points.push_back(make_pair(point1, point2));
	}

}

void ColorMarkersDetctor::init_lower_upper_ranges() {
	//init ranges from file
	read_ranges();
	get_lower_upper_ranges();
}

void ColorMarkersDetctor::get_lower_upper_ranges() {
	double min_h = 256;
	double min_s = 256;
	double min_v = 256;
	double max_h = -1;
	double max_s = -1;
	double max_v = -1;
	for (int x = 0; x < upper_ranges.size(); x++) {
		Scalar up = upper_ranges[x];
		Scalar lo = lower_ranges[x];

		if (lo[0] < min_h)
			min_h = lo[0];
		if (up[0] > max_h)
			max_h = up[0];
		//}

		if (lo[1] < min_s)
			min_s = lo[1];
		if (up[1] > max_s)
			max_s = up[1];

		if (lo[2] < min_v)
			min_v = lo[2];
		if (up[2] > max_v)
			max_v = up[2];
	}

	lower_range = cv::Scalar(min_h - (tolerance / 2), min_s - (tolerance / 2),
			min_v - tolerance);
	upper_range = cv::Scalar(max_h + (tolerance / 2), max_s + (tolerance / 2),
			max_v + tolerance);
	//std::cout << "Global Range: " << lower_range << ", " << upper_range
	//		<< std::endl;
}

bool ColorMarkersDetctor::calibrate(Mat frame_o, Mat right) {
	bool result = true;
	string window_name1 = "Select Image";
	string window_name2 = "Calibration Blurred";
	string window_name3 = "Calibration HSV ";
	string window_name4 = "Select Markers";
	string window_name5 = "Illumination Normalized";
	string window_name6 = "Illumination Normalized 2";
	string window_name7 = "Combined";

	//init ranges from file
	/*lower_ranges = Configuration::Instance()->get_lower_ranges();
	 upper_ranges = Configuration::Instance()->get_upper_ranges();
	 */

	frame_o = normalize_image(frame_o);
	right = normalize_image(right);
	Size sz1 = frame_o.size();
	Size sz2 = right.size();
	combined_frame = Mat(sz1.height, sz1.width + sz2.width, frame_o.type());
	frame_o.copyTo(combined_frame(Rect(0, 0, sz1.width, sz1.height)));
	right.copyTo(combined_frame(Rect(sz1.width, 0, sz2.width, sz2.height)));
	calib_frame = combined_frame.clone();

	//select image to be used
	while (true) {
		imshow(window_name1, combined_frame);
		int keyPressed = waitKey(1);
		if (keyPressed == 1048686) //n
			return true;
		else if (keyPressed == 1048586) //enter
			break;
		else if (keyPressed == 1048603) {
			get_lower_upper_ranges();
			destroyWindow(window_name1);
			return false;
		}

	}
	destroyWindow(window_name1);
	cout << "Image has been selected" << endl;
	cout << "Select markers using the mouse, press esc to quit this process"
			<< endl;

	int count = 0;
	while (true) {
		//set the callback function for any mouse event
		setMouseCallback(window_name4, mouseCallback, this);
		//show the image
		imshow(window_name4, calib_frame);
		// Wait until user press some key
		int keyPressed = waitKey(1);
		if (keyPressed == 1048603)
			break;
		count++;
	}

	//GaussianBlur(frame, frame, Size(15, 15), 0, 0);
	//imshow(window_name2, frame);
	Mat imgHSV, out;
	cvtColor(combined_frame, imgHSV, color_space);
	imshow(window_name3, imgHSV);
	Mat f_image;
	f_image = combined_frame;

	for (int x = 0; x < calibraition_points.size(); x++) {
//compute image HSV range
		int min_h = 256;
		int min_s = 256;
		int min_v = 256;
		int max_h = -1;
		int max_s = -1;
		int max_v = -1;

		/* loop ROI and get min and max HSV values */
		Point point1 = calibraition_points[x].first, point2 =
				calibraition_points[x].second;
		for (int i = point1.y; i < point2.y; i++) {

			for (int j = point1.x; j < point2.x; j++) {
				cv::Vec3b hsv = imgHSV.at<cv::Vec3b>(i, j);
				//cout << hsv << endl;
				//if (hsv.val[0] > 1) // if not black  (possible point of failure - what if ball is black?)
				//		{
				if (hsv.val[0] < min_h)
					min_h = hsv.val[0];
				if (hsv.val[0] > max_h)
					max_h = hsv.val[0];
				//}
				if (hsv.val[1] > 1) {
					if (hsv.val[1] < min_s)
						min_s = hsv.val[1];
					if (hsv.val[1] > max_s)
						max_s = hsv.val[1];
				}
				if (hsv.val[2] > 1) {
					if (hsv.val[2] < min_v)
						min_v = hsv.val[2];
					if (hsv.val[2] > max_v)
						max_v = hsv.val[2];
				}
			}
		}
		lower_range = cv::Scalar(min_h, min_s - (tolerance / 2),
				min_v - tolerance);
		upper_range = cv::Scalar(max_h, max_s + (tolerance / 2),
				max_v + tolerance);
		std::cout << "Range: " << lower_range << ", " << upper_range
				<< std::endl;

		upper_ranges.push_back(upper_range);
		lower_ranges.push_back(lower_range);

	}

	get_lower_upper_ranges();

	Mat combined_image = Mat::zeros(imgHSV.size(), 0);

	//for (int x = 0; x < upper_ranges.size(); x++) {

	inRange(imgHSV, lower_range, upper_range, out);

	//imshow(window_name4, out);
	combined_image = combined_image + out;

	//}

	imshow(window_name7, combined_image);
	while (true) {
		int keyPressed = cv::waitKey(1);
		if (keyPressed == 1048603) //esc
			break;
	}

	save_ranges(upper_ranges, lower_ranges);

	destroyWindow(window_name2);
	destroyWindow(window_name3);
	destroyWindow(window_name4);
	destroyWindow(window_name7);

	return true;

}

vector<Point2f> ColorMarkersDetctor::detect_markers_moments(Mat & frame,
		Mat & mask) {
	Mat hsv;
	Mat morpho_kernell = getStructuringElement(MORPH_ELLIPSE, Size(11, 11));

	frame = normalize_image(frame);
	cv::cvtColor(frame, hsv, color_space);
	cv::inRange(hsv, lower_range, upper_range, mask);

	morphologyEx(mask, mask, MORPH_CLOSE, morpho_kernell);

	Mat canny_output;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	vector<Point2f> centers;

// Detect edges using canny
// Find contours
	findContours(mask.clone(), contours, hierarchy, CV_RETR_EXTERNAL,
			CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

	if (contours.size() > 0) {
		//cout << "Contours: " << contours.size() << endl;
		for (int i = 0; i < contours.size(); i++) {
			//vector<Point> cnt = contours[i];
			Point2f center;
			float current_radious;
			Moments mu;
			minEnclosingCircle((Mat) contours[i], center, current_radious);
			mu = moments(contours[i], true);
			Point2f center_m(mu.m10 / mu.m00, mu.m01 / mu.m00);

			if (current_radious > radius) {
				//cout << "Center: " << center << endl;
				//cout << "Center_m: " << center_m << endl << "****" << endl;
				circle(frame, center, current_radious, Scalar(0, 0, 255), 2,
						LINE_AA);
				circle(frame, center_m, 3, Scalar(0, 255, 255), -1, LINE_AA);
				centers.push_back(center_m);
			}
		}

		cout << "Detected markers: " << centers.size() << endl;
	}

	return centers;

}

vector<Point> ColorMarkersDetctor::detect_markers_clustering(Mat & frame,
		Mat & mask) {
	Mat hsv;
	Mat morpho_kernell = getStructuringElement(MORPH_ELLIPSE, Size(11, 11));

	frame = normalize_image(frame);
	cv::cvtColor(frame, hsv, color_space);
	cv::inRange(hsv, lower_range, upper_range, mask);

	morphologyEx(mask, mask, MORPH_CLOSE, morpho_kernell);

	Mat canny_output;
	vector<Point> points;
	findNonZero(mask, points);
	int euclidean_distance = 30;
	vector<int> labels;
	vector<Point2f> centers;

	int n_labels = partition(points, labels, Predicate(euclidean_distance));

// Store all points in same cluster, and compute centroids
	vector<vector<Point>> clusters(n_labels);
	vector<Point> centroids(n_labels, Point(0, 0));

	for (int i = 0; i < points.size(); ++i) {
		clusters[labels[i]].push_back(points[i]);
		centroids[labels[i]] += points[i];
	}
	for (int i = 0; i < n_labels; ++i) {
		centroids[i].x /= clusters[i].size();
		centroids[i].y /= clusters[i].size();
	}
// Draw centroids
	for (int i = 0; i < n_labels; ++i) {
		circle(frame, centroids[i], 3, Scalar(0, 0, 255), CV_FILLED);
		circle(frame, centroids[i], 6, Scalar(0, 255, 255));
	}
	//cout << "Detected markers: " << centroids.size() << endl;

	return centroids;

}

void ColorMarkersDetctor::compute_centers(Mat& ref, Mat& result,
		vector<Point2f> &centers, vector<float> &radius_vec, Mat& disp) {
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	// Find contours

	findContours(ref.clone(), contours, hierarchy, CV_RETR_EXTERNAL,
			CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

	if (contours.size() > 0) {

		stereoMatcher.compute_3d_image(disp);

		for (int i = 0; i < contours.size(); i++) {
			//vector<Point> cnt = contours[i];
			Point2f center;
			float current_radious;
			Moments mu;
			minEnclosingCircle((Mat) contours[i], center, current_radious);
			if (current_radious > radius
					&& stereoMatcher.is_valid_point(disp, center)) {
				circle(result, center, current_radious, Scalar(78, 175, 255),
				CV_FILLED);
				centers.push_back(center);
				radius_vec.push_back(current_radious);
			}
		}
		//cout << "Detected contours: " << centers.size() << endl;
	}
}

vector<Point> ColorMarkersDetctor::detect_markers_combined(Mat & frame,
		Mat &right_converted, Mat & mask_a, Mat & mask_b, Mat &mask_c,
		Mat& depth_image) {
	Mat hsv, hsv_right, out_range, out_right_range;
	Mat morpho_kernell = getStructuringElement(MORPH_ELLIPSE, Size(11, 11));

	frame = normalize_image(frame);
	right_converted = normalize_image(right_converted);

	cv::cvtColor(frame, hsv, color_space);
	cv::cvtColor(right_converted, hsv_right, color_space);


	//mask_a = Mat::zeros(frame.size(), 0);
	//mask_c = Mat::zeros(frame.size(), 0);

	//for (int x = 0; x < upper_ranges.size(); x++) {
	cv::inRange(hsv, lower_range, upper_range, mask_a);
	cv::inRange(hsv_right, lower_range, upper_range, mask_c);
	//mask_c = mask_c + out_right_range;
	//mask_a = mask_a + out_range;
	//}

	morphologyEx(mask_a, mask_a, MORPH_CLOSE, morpho_kernell);
	morphologyEx(mask_c, mask_c, MORPH_CLOSE, morpho_kernell);


	vector<Point2f> centers, centers_rigth;
	vector<float> radius_vec_left, radius_vec_right;
	vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;
	mask_b = Mat::zeros(mask_a.size(), mask_a.type());
	Mat mask_d = Mat::zeros(mask_a.size(), mask_a.type());

	compute_centers(mask_a, mask_b, centers, radius_vec_left, depth_image);
	compute_centers(mask_c, mask_d, centers_rigth, radius_vec_right,
			depth_image);
	vector<Point2f> points_right_on_left;

	//stereoMatcher.match_points_rigth_image_to_left_image(centers_rigth,
	//		points_right_on_left);

	for (int i = 0; i < centers_rigth.size(); i++) {
		//vector<Point> cnt = contours[i];
		Point2f center = centers_rigth[i];

		circle(mask_b, center, radius_vec_right[i], Scalar(255, 0, 0),
		CV_FILLED);

	}



	/*Possible redundant code*/
	findContours(mask_b.clone(), contours, hierarchy, CV_RETR_EXTERNAL,
			CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
	vector<Point> points;
	for (int x = 0; x < contours.size(); x++) {
		for (int y = 0; y < contours[x].size(); y++) {
			points.push_back(contours[x][y]);
		}
	}

	//findNonZero(mask_b, points);

	vector<int> labels;
// With lambda function
	int n_labels = partition(points, labels, Predicate(euclidean_distance));

// Store all points in same cluster, and compute centroids

	vector<vector<Point>> clusters(n_labels);
	vector<Point> centroids(n_labels, Point(0, 0));

	for (int i = 0; i < points.size(); ++i) {
		clusters[labels[i]].push_back(points[i]);
		centroids[labels[i]] += points[i];
	}
	for (int i = 0; i < n_labels; ++i) {
		centroids[i].x /= clusters[i].size();
		centroids[i].y /= clusters[i].size();
	}
// Draw centroids
	for (int i = 0; i < n_labels; ++i) {
		circle(frame, centroids[i], 3, Scalar(0, 0, 255), CV_FILLED);
		circle(frame, centroids[i], 6, Scalar(0, 255, 255));
	}
	//cout << "Detected markers: " << centroids.size() << endl;

	return centroids;

}
ColorMarkersDetctor::ColorMarkersDetctor(string ranges_dir) :
		radius(
				Configuration::Instance()->get_minimum_valid_detected_marker_radius()), click_one(
				false), click_two(false), tolerance(
				Configuration::Instance()->get_calibration_tolerance()), calibration_done(
				false), drag(false), color_space(CV_BGR2HSV), euclidean_distance(
				Configuration::Instance()->get_min_euclidean_dist_between_markers()), ranges_dir(
				ranges_dir), upper_scalar_string("Uppercalar"), lower_scalar_string(
				"LowerScalar"), num_ranges_string("nRanges"), ranges_file(
				"ranges.xml") {

	//cout << "Working with tolerance: " << tolerance << endl;

	read_ranges();
}

void ColorMarkersDetctor::compute_from_videocap(int camera_id) {
	VideoCapture cap(camera_id); // open the default camera
	if (!cap.isOpened()) // check if we succeeded
		return;
	Mat frame, mask;

	while (true) {
		cap >> frame;
		cout << "Press n to go to next frame" << endl;
		if (calibrate(frame, frame))
			break;
	}
	while (true) {
		cap >> frame;

		detect_markers_moments(frame, mask);
		int keyPressed = cv::waitKey(1);
		if (keyPressed == 1048603)
			break;
	}
}

