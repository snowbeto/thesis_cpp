#include <camera_parameters.hpp>
#include<fstream>
#include<iomanip>

CameraParameters* CameraParameters::instance = NULL;

void create_latex_matrix(Mat aMat, string name, ofstream& file) {

	file << "\\begin{equation}" << name << " = \\begin{bmatrix}";
	for (int j = 0; j < aMat.rows; j++) {
		for (int i = 0; i < aMat.cols; i++) {
			file << std::fixed << std::setprecision(3) << aMat.at<double>(j, i)
					<< " ";
			if (i < aMat.cols - 1)
				file << "&";
		}
		if (j < aMat.rows - 1)
			file << "\\\\";
		file << " \n";
	}

	file << "\\end{bmatrix} \\\\ \\end{equation}  \n";

}

CameraParameters::CameraParameters() :
		calibration_files_path("./data/calibration/"), intrinsic_filename(
				"intrinsics_lp.yml"), extrinsic_filename("extrinsics_lp.yml"), homography_filename(
				"homography_lp.yml"), intrinsic_filename_lp(
				"intrinsics_lp.yml"), extrinsic_filename_lp(
				"extrinsics_lp.yml"), homography_filename_lp(
				"homography_lp.yml"), resize_size(640, 360) {
	//File storing the latex verions of the camera parameters
	ofstream myfile;
	myfile.open("./data/calibration/latex_camera_parameters.txt");
	myfile << "Intrinsic: \n";

	if (!intrinsic_filename.empty()) {
		// reading intrinsic parameters
		FileStorage fs(calibration_files_path + intrinsic_filename,
				FileStorage::READ);
		if (!fs.isOpened()) {
			printf("Failed to open file %s\n",
					(calibration_files_path + intrinsic_filename).c_str());
		}

		fs["M1"] >> M1;
		create_latex_matrix(M1, "M1", myfile);
		fs["D1"] >> D1;
		create_latex_matrix(D1, "D1", myfile);
		fs["M2"] >> M2;
		create_latex_matrix(M2, "M2", myfile);
		fs["D2"] >> D2;
		create_latex_matrix(D2, "D2", myfile);
		fs["imgSize"] >> img_size;

		fs.open(calibration_files_path + extrinsic_filename, FileStorage::READ);
		if (!fs.isOpened()) {
			printf("Failed to open file %s\n",
					(calibration_files_path + extrinsic_filename).c_str());
		}
		myfile << "Extrinsic: \n";
		fs["R"] >> R;
		create_latex_matrix(R, "R", myfile);
		fs["T"] >> T;
		create_latex_matrix(T, "T", myfile);
		fs["F"] >> F;
		create_latex_matrix(F, "F", myfile);

		stereoRectify(M1, D1, M2, D2, img_size, R, T, R1, R2, P1, P2, Q,
				CALIB_ZERO_DISPARITY, -1, img_size, &roi1, &roi2);

		initUndistortRectifyMap(M1, D1, R1, P1, img_size, CV_16SC2, map11,
				map12);
		initUndistortRectifyMap(M2, D2, R2, P2, img_size, CV_16SC2, map21,
				map22);

		create_latex_matrix(R1, "R1", myfile);
		create_latex_matrix(P1, "P1", myfile);
		create_latex_matrix(R2, "R2", myfile);
		create_latex_matrix(P2, "P2", myfile);
		create_latex_matrix(Q, "Q", myfile);

	}

	if (!intrinsic_filename.empty()) {
		// reading intrinsic parameters
		FileStorage fs(calibration_files_path + intrinsic_filename_lp,
				FileStorage::READ);
		if (!fs.isOpened()) {
			printf("Failed to open file %s\n",
					(calibration_files_path + intrinsic_filename_lp).c_str());
		}

		fs["M1"] >> M1_lp;
		fs["D1"] >> D1_lp;
		fs["M2"] >> M2_lp;
		fs["D2"] >> D2_lp;
		fs["imgSize"] >> img_size_lp;

		fs.open(calibration_files_path + extrinsic_filename_lp,
				FileStorage::READ);
		if (!fs.isOpened()) {
			printf("Failed to open file %s\n",
					(calibration_files_path + extrinsic_filename_lp).c_str());
		}

		fs["R"] >> R_lp;
		fs["T"] >> T_lp;
		fs["F"] >> F_lp;

		stereoRectify(M1_lp, D1_lp, M2_lp, D2_lp, img_size_lp, R_lp, T_lp,
				R1_lp, R2_lp, P1_lp, P2_lp, Q_lp, CALIB_ZERO_DISPARITY, -1,
				img_size_lp, &roi1_lp, &roi2_lp);

		initUndistortRectifyMap(M1_lp, D1_lp, R1_lp, P1_lp, img_size_lp,
		CV_16SC2, map11_lp, map12_lp);
		initUndistortRectifyMap(M2_lp, D2_lp, R2_lp, P2_lp, img_size_lp,
		CV_16SC2, map21_lp, map22_lp);

	}

	if (!homography_filename.empty()) {
		FileStorage fs(calibration_files_path + homography_filename,
				FileStorage::READ);
		if (!fs.isOpened()) {
			cout << "Could not open homography file" << endl;
		}

		fs["H"] >> H;
	}

	if (!homography_filename_lp.empty()) {
		FileStorage fs(calibration_files_path + homography_filename_lp,
				FileStorage::READ);
		if (!fs.isOpened()) {
			cout << "Could not open homography_lp file" << endl;
		}

		fs["H"] >> H_lp;
	}

	myfile << "Homography: \n";
	create_latex_matrix(H, "H", myfile);

	myfile.close();

}
