/*
 *  stereo_match.cpp
 *  calibration
 *
 *  Created by Victor  Eruhimov on 1/18/10.
 *  Copyright 2010 Argus Corp. All rights reserved.
 *
 */

#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/core/utility.hpp"
#include "opencv2/ximgproc/disparity_filter.hpp"

#include <stdio.h>

using namespace cv;
using namespace cv::ximgproc;

static void print_help() {
	printf(
			"\nDemo stereo matching converting L and R images into disparity and point clouds\n");
	printf(
			"\nUsage: stereo_match <left_image> <right_image> [--algorithm=bm|sgbm|hh|sgbm3way] [--blocksize=<block_size>]\n"
					"[--max-disparity=<max_disparity>] \n");
}

static void create_pcl_file(const char* filename, const Mat& mat) {
	const double max_z = 1.0e4;
	std::vector<Vec3f> points;
	for (int y = 0; y < mat.rows; y++) {
		for (int x = 0; x < mat.cols; x++) {
			Vec3f point = mat.at<Vec3f>(y, x);
			if (fabs(point[2] - max_z) < FLT_EPSILON || fabs(point[2]) > max_z)
				continue;
			points.push_back(point);
		}

	}

	FILE* fp = fopen(filename, "wt");
	fprintf(fp, "VERSION .7\n");
	fprintf(fp, "FIELDS x y z\n");
	fprintf(fp, "SIZE 4 4 4\n");
	fprintf(fp, "TYPE F F F\n");
	fprintf(fp, "COUNT 1 1 1\n");
	fprintf(fp, "WIDTH %d\n", (int) points.size());
	fprintf(fp, "HEIGHT 1\n");
	fprintf(fp, "VIEWPOINT 0 0 0 1 0 0 0\n");
	fprintf(fp, "POINTS  %d\n", (int) points.size());
	fprintf(fp, "DATA ascii\n");

	for (int y = 0; y < points.size(); y++) {
		Vec3f point = points[y];
		fprintf(fp, "%f %f %f\n", point[0], point[1], point[2]);
	}
	fclose(fp);
}
enum {
	STEREO_3WAY = 4
};

int main(int argc, char** argv) {
	std::string img1_filename = "";
	std::string img2_filename = "";
	std::string intrinsic_filename = "intrinsics.yml";
	std::string extrinsic_filename = "extrinsics.yml";
	std::string disparity_filename = "disparity_image.jpg";
	std::string point_cloud_filename = "./my_pcl.pcd";
	Ptr<DisparityWLSFilter> wls_filter;
	int alg = STEREO_3WAY;
	int SADWindowSize = 3, numberOfDisparities = 64;
	bool no_display = false;
	float scale = 1.0;

	//Ptr<StereoBM> left_bm = StereoBM::create(16, 9);
	Ptr<StereoSGBM> left_sgbm = StereoSGBM::create(0, 16, 3);
	cv::CommandLineParser parser(argc, argv,
			"{@arg1||}{@arg2||}{help h||}{max-disparity|0|}{blocksize|0|}");
	if (parser.has("help")) {
		print_help();
		return 0;
	}
	img1_filename = parser.get<std::string>(0);
	img2_filename = parser.get<std::string>(1);

	if (numberOfDisparities < 1 || numberOfDisparities % 16 != 0) {
		printf(
				"Command-line parameter error: The max disparity (--maxdisparity=<...>) must be a positive integer divisible by 16\n");
		print_help();
		return -1;
	}
	if (SADWindowSize < 1 || SADWindowSize % 2 != 1) {
		printf(
				"Command-line parameter error: The block size (--blocksize=<...>) must be a positive odd number\n");
		return -1;
	}
	if (img1_filename.empty() || img2_filename.empty()) {
		printf(
				"Command-line parameter error: both left and right images must be specified\n");
		return -1;
	}

	Mat left = imread(img1_filename);
	Mat right = imread(img2_filename);
	Mat left_for_matcher, right_for_matcher;

	if (left.empty()) {
		printf(
				"Command-line parameter error: could not load the first input image file\n");
		return -1;
	}
	if (right.empty()) {
		printf(
				"Command-line parameter error: could not load the second input image file\n");
		return -1;
	}

	Size img_size = left.size();

	Rect roi1, roi2;
	Mat Q;

	if (!intrinsic_filename.empty()) {
		// reading intrinsic parameters
		FileStorage fs(intrinsic_filename, FileStorage::READ);
		if (!fs.isOpened()) {
			printf("Failed to open file %s\n", intrinsic_filename.c_str());
			return -1;
		}

		Mat M1, D1, M2, D2;
		fs["M1"] >> M1;
		fs["D1"] >> D1;
		fs["M2"] >> M2;
		fs["D2"] >> D2;

		M1 *= scale;
		M2 *= scale;

		fs.open(extrinsic_filename, FileStorage::READ);
		if (!fs.isOpened()) {
			printf("Failed to open file %s\n", extrinsic_filename.c_str());
			return -1;
		}

		Mat R, T, R1, P1, R2, P2;
		fs["R"] >> R;
		fs["T"] >> T;

		stereoRectify(M1, D1, M2, D2, img_size, R, T, R1, R2, P1, P2, Q,
				CALIB_ZERO_DISPARITY, -1, img_size, &roi1, &roi2);

		Mat map11, map12, map21, map22;
		initUndistortRectifyMap(M1, D1, R1, P1, img_size, CV_16SC2, map11,
				map12);
		initUndistortRectifyMap(M2, D2, R2, P2, img_size, CV_16SC2, map21,
				map22);

		Mat leftr, rightr;
		remap(left, leftr, map11, map12, INTER_LINEAR);
		remap(right, rightr, map21, map22, INTER_LINEAR);

		left = leftr;
		right = rightr;
		imwrite("./leftSR.jpg", left);
		imwrite("./rightSR.jpg", right);
	}

	left_for_matcher = left.clone();
	right_for_matcher = right.clone();
	Mat left_disp, right_disp;
	Mat filtered_disp;
	Mat conf_map = Mat(left.rows, left.cols, CV_8U);
	double vis_mult = 1.0;
	conf_map = Scalar(255);
	Rect ROI;
	numberOfDisparities =
			numberOfDisparities > 0 ?
					numberOfDisparities : ((img_size.width / 8) + 15) & -16;

	left_sgbm->setPreFilterCap(63);
	int sgbmWinSize = SADWindowSize > 0 ? SADWindowSize : 3;
	left_sgbm->setBlockSize(sgbmWinSize);

	int cn = left.channels();

	left_sgbm->setP1(8 * cn * sgbmWinSize * sgbmWinSize);
	left_sgbm->setP2(32 * cn * sgbmWinSize * sgbmWinSize);
	left_sgbm->setMinDisparity(0);
	left_sgbm->setNumDisparities(numberOfDisparities);
	left_sgbm->setUniquenessRatio(10);
	left_sgbm->setSpeckleWindowSize(100);
	left_sgbm->setSpeckleRange(32);
	left_sgbm->setDisp12MaxDiff(1);
	left_sgbm->setMode(StereoSGBM::MODE_SGBM_3WAY);

	float t = getTickCount();
	wls_filter = createDisparityWLSFilter(left_sgbm);
	Ptr<StereoMatcher> right_matcher = createRightMatcher(left_sgbm);

	cvtColor(left_for_matcher, left_for_matcher, COLOR_BGR2GRAY);
	cvtColor(right_for_matcher, right_for_matcher, COLOR_BGR2GRAY);

	left_sgbm->compute(left_for_matcher, right_for_matcher, left_disp);
	right_matcher->compute(right_for_matcher, left_for_matcher, right_disp);

	t = getTickCount() - t;
	printf("Time elapsed: %fms\n", t * 1000 / getTickFrequency());

	wls_filter->setLambda(8000);
	wls_filter->setSigmaColor(1.5);
	t = getTickCount();
	wls_filter->filter(left_disp, left, filtered_disp, right_disp);
	t = getTickCount() - t;
	printf("Filtering time: %fms\n", t * 1000 / getTickFrequency());
	//! [filtering]

	// Get the ROI that was used in the last filter call:
	ROI = wls_filter->getROI();

	namedWindow("left", 1);
	imshow("left", left);
	namedWindow("right", 1);
	imshow("right", right);
	Mat raw_disp_vis;
	getDisparityVis(left_disp, raw_disp_vis, vis_mult);
	namedWindow("raw disparity", WINDOW_AUTOSIZE);
	imshow("raw disparity", raw_disp_vis);
	Mat filtered_disp_vis;
	getDisparityVis(filtered_disp, filtered_disp_vis, vis_mult);
	namedWindow("filtered disparity", WINDOW_AUTOSIZE);
	imshow("filtered disparity", filtered_disp_vis);
	waitKey();

	printf("storing the point cloud...");
	fflush(stdout);
	Mat xyz;
	reprojectImageTo3D(filtered_disp_vis, xyz, Q, true);
	create_pcl_file(point_cloud_filename.c_str(), xyz);
	printf("\n");

	imwrite(disparity_filename, filtered_disp_vis);
	printf("\n");

	return 0;
}
