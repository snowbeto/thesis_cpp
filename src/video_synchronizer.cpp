#include <stereo_disparity_creator.hpp>
#include <color_markers_detector.hpp>
#include <visualizer.hpp>
#include <analyzer.hpp>
#include <sony_camera.hpp>
#include <human_detector.hpp>
#include <configuration.hpp>
#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>
#include <camera_parameters.hpp>

void init() {
	Configuration::Instance();
	CameraParameters::Instance();
}

void merge_left_right_frames(Mat& l, Mat& r, Mat & combined_frame) {
	Size sz1 = l.size();
	Size sz2 = r.size();
	combined_frame = Mat(sz1.height, sz1.width + sz2.width, l.type());
	l.copyTo(combined_frame(Rect(0, 0, sz1.width, sz1.height)));
	r.copyTo(combined_frame(Rect(sz1.width, 0, sz2.width, sz2.height)));

}

bool readXMLStringList(const string& filename, vector<string>& list) {
	list.resize(0);
	FileStorage fs(filename, FileStorage::READ);
	if (!fs.isOpened())
		return false;
	FileNode n = fs.getFirstTopLevelNode();
	if (n.type() != FileNode::SEQ)
		return false;
	FileNodeIterator it = n.begin(), it_end = n.end();
	for (; it != it_end; ++it)
		list.push_back((string) *it);
	return true;
}

void resize_image(Mat& image) {
	resize(image, image, CameraParameters::Instance()->get_resize_size());
}

void save_videos(VideoCapture& left_video, VideoCapture& right_video,
		vector<Mat> & left_video_frames, int left_start_index,
		string left_video_name, vector<Mat> & right_video_frames,
		int right_start_index, string right_video_name, Size& frame_size)

		{

	std::string s = ".MP4";

	std::string::size_type i = left_video_name.find(s);

	if (i != std::string::npos)
		left_video_name.erase(i, s.length());

	i = right_video_name.find(s);

	if (i != std::string::npos)
		right_video_name.erase(i, s.length());

	VideoWriter left_writer(left_video_name + ".avi",
			cv::VideoWriter::fourcc('X', 'V', 'I', 'D'),
			left_video.get(cv::CAP_PROP_FPS), frame_size);
	VideoWriter right_writer(right_video_name + ".avi",
			cv::VideoWriter::fourcc('X', 'V', 'I', 'D'),
			right_video.get(cv::CAP_PROP_FPS), frame_size);

	for (int i = left_start_index; i < left_video_frames.size(); i++) {
		left_writer << left_video_frames[i];
	}

	for (int i = right_start_index; i < right_video_frames.size(); i++) {
		right_writer << right_video_frames[i];
	}

	left_video.release();
	right_video.release();
}

void reproduce_videos() {
	string windowName1 = "Videos";

	vector<string> video_dirs_list;

	video_dirs_list.push_back(Configuration::Instance()->get_svn_train_dir());
	video_dirs_list.push_back(Configuration::Instance()->get_svn_predict_dir());

	for (int index = 0; index < video_dirs_list.size(); index++) {
		/*open subject dir*/
		std::ifstream vid_dirs(
				video_dirs_list[index]
						+ Configuration::Instance()->get_vid_dirs_file());
		std::string dir, subdir, video;
		Size frame_size;
		while (std::getline(vid_dirs, dir)) {
			string trannig_dir = video_dirs_list[index] + dir + "/";
			vector<string> video_list;
			string vid_file = trannig_dir
					+ Configuration::Instance()->get_vid_list_file();
			cout << "Reading files from: " << trannig_dir << endl;

			readXMLStringList(vid_file, video_list);
			int num_vids = (int) video_list.size() / 2; // temporary fix to deal with different directories

			bool calibrate_flag =
					Configuration::Instance()->get_calibrate_maker_color();

			for (int i = 0; i < num_vids; i++) {
				string left_video_name = trannig_dir + video_list[i * 2];
				string right_video_name = trannig_dir + video_list[i * 2 + 1];

				VideoCapture left_video(left_video_name);
				VideoCapture right_video(right_video_name);
				int left_frames = left_video.get(cv::CAP_PROP_FRAME_COUNT);
				int right_frames = right_video.get(cv::CAP_PROP_FRAME_COUNT);

				if (right_frames != left_frames) {
					cout << "Synchronizing frames" << endl;
					int diff = right_frames - left_frames;
					cout << "Frames in right video: " << right_frames << endl;
					cout << "Frames in left video: " << left_frames << endl;

					if (diff > 0) {
						cout << "Right video has more frames "
								<< right_video_name << endl;

					} else {
						cout << "Left video has more frames" << left_video_name
								<< endl;
					}
				}

				vector<Mat> left_video_frames, right_video_frames;
				left_video_frames.reserve(500000);
				right_video_frames.reserve(500000);

				cout << "Loading videos ..." << endl;
				while (true) {

					Mat frame;
					left_video >> frame;
					if (frame.empty())
						break;

					resize_image(frame);
					frame_size = frame.size();

					left_video_frames.push_back(frame);

				}

				while (true) {
					Mat frame;
					right_video >> frame;

					if (frame.empty())
						break;
					resize_image(frame);
					right_video_frames.push_back(frame);

				}

				cout << "Done" << endl;

				Mat left, right;

				bool loop = true;
				bool advance_right = true;
				bool advance_left = true;
				int left_counter = 0;
				int right_counter = 0;

				while (loop) {
					if (advance_left) {
						if (left_counter < 0)
							left_counter = 0;
						if (left_counter >= left_video_frames.size())
							left_counter = left_video_frames.size() - 1;

						left = left_video_frames[left_counter];
					}
					if (advance_right) {
						if (right_counter < 0)
							right_counter = 0;
						if (right_counter >= right_video_frames.size())
							right_counter = right_video_frames.size() - 1;
						right = right_video_frames[right_counter];
					}
					cout << "right counter " << right_counter << endl;
					cout << "left counter " << left_counter << endl;

					if (left.empty())
						break;

					//resize_image(left);
					//resize_image(right);

					Mat combined_image;
					merge_left_right_frames(left, right, combined_image);

					while (true) {
						imshow(windowName1, combined_image);

						int keyPressed = 1048586;//cv::waitKey(1);

						if (keyPressed != -1)
							cout << keyPressed << endl;

						if (keyPressed == 1048690) //r
								{
							advance_right = true;
							advance_left = false;
							right_counter++;
							break;
						}

						if (keyPressed == 1048677) //e
								{
							advance_right = true;
							advance_left = false;
							right_counter--;

							break;
						}

						if (keyPressed == 1048684) //l
								{
							left_counter++;
							advance_right = false;
							advance_left = true;
							break;
						}

						if (keyPressed == 1048683) //k
								{
							left_counter--;
							advance_right = false;
							advance_left = true;
							break;
						}

						if (keyPressed == 1048586) // enter
								{

							cout << "Saving video .." << endl;
							save_videos(left_video, right_video,
									left_video_frames, left_counter,
									left_video_name, right_video_frames,
									right_counter, right_video_name,
									frame_size);
							;

							cout << "Done..." << endl;
							loop = false;
							break;
						}

						if (keyPressed == 1048686) //n
								{
							right_counter++;
							left_counter++;
							advance_right = true;
							advance_left = true;
							break;
						}

						if (keyPressed == 1048674) //b
								{
							right_counter--;
							left_counter--;
							advance_right = true;
							advance_left = true;
							break;
						}

						if (keyPressed == 1048603) {

							loop = false;
							break;
						}
					}

				}
				destroyWindow(windowName1);
			}

		}

	}

}

int main() {
	init();
	reproduce_videos();

	return 0;

}
