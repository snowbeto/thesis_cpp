#include "opencv2/opencv.hpp"
#include<cmath>
#include <string>
#include "iostream"

using namespace cv;
using namespace std;

class ColorMarkersDetctor {
public:
	/*Code copied from the internet*/
	Mat normalize_image(Mat bgr_image) {
		// READ RGB color image and convert it to Lab
		cv::Mat lab_image;
		cv::cvtColor(bgr_image, lab_image, CV_BGR2Lab);

		// Extract the L channel
		std::vector<cv::Mat> lab_planes(3);
		cv::split(lab_image, lab_planes); // now we have the L image in lab_planes[0]

		// apply the CLAHE algorithm to the L channel
		cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
		clahe->setClipLimit(2);
		cv::Mat dst;
		clahe->apply(lab_planes[0], dst);

		// Merge the the color planes back into an Lab image
		dst.copyTo(lab_planes[0]);
		cv::merge(lab_planes, lab_image);

		// convert back to RGB
		cv::Mat image_clahe;
		cv::cvtColor(lab_image, image_clahe, CV_Lab2BGR);

		return image_clahe;
	}

	Mat normalize_image_2(Mat image) {
		Mat dst;
		cvtColor(image, image, CV_BGR2GRAY);
		equalizeHist(image, dst);
		cvtColor(dst, image, CV_GRAY2BGR);
		return image;

	}
	static void mouseCallback(int event, int x, int y, int flags, void *param) {
		ColorMarkersDetctor *self = static_cast<ColorMarkersDetctor*>(param);
		self->callbackFunc(event, x, y, flags);
	}
	void callbackFunc(int event, int x, int y, int flags) {
		if (event == EVENT_LBUTTONDOWN) {
			if (!click_one) {
				point1.x = x;
				point1.y = y;
				click_one = true;
				cout << "point1 " << endl;
			} else if (!click_two) {
				point2.x = x;
				point2.y = y;
				click_two = true;
				cout << "Point2" << endl;
			}
		}
	}

	bool calibrate(Mat frame, Mat right) {
		bool result = true;
		string window_name1 = "Calibration";
		string window_name2 = "Calibration Blurred";
		string window_name3 = "Calibration HSV ";
		string window_name4 = "Calibration Result";
		string window_name5 = "Illumination Normalized";
		string window_name6 = "Illumination Normalized 2";
		string window_name7 = "Combined";
		namedWindow(window_name1, 1);

		int area = (2 * radius + 1) * (2 * radius + 1);
		frame = normalize_image(frame);
		//select the region on the image
		while (!click_one || !click_two) {
			//set the callback function for any mouse event
			setMouseCallback(window_name1, mouseCallback, this);
			//show the image

			imshow(window_name1, frame);

			// Wait until user press some key
			int keyPressed = waitKey(10);
			if (keyPressed == 1048603)
				return false;
		}

		GaussianBlur(frame, frame, Size(15, 15), 0, 0);
		imshow(window_name2, frame);
		Mat imgHSV, out;
		cvtColor(frame, imgHSV, CV_BGR2HLS);
		imshow(window_name3, imgHSV);
		Mat f_image;
		f_image = frame;

		//compute image HSV range
		int min_h = 256;
		int min_s = 256;
		int min_v = 256;
		int max_h = -1;
		int max_s = -1;
		int max_v = -1;

		/* loop ROI and get min and max HSV values */

		for (int i = point1.y; i < point2.y; i++) {
			for (int j = point1.x; j < point2.x; j++) {
				cv::Vec3b hsv = imgHSV.at<cv::Vec3b>(i, j);
				if (hsv.val[2] != 0) // if not black  (possible point of failure - what if ball is black?)
						{
					if (hsv.val[0] < min_h)
						min_h = hsv.val[0];
					if (hsv.val[1] < min_s)
						min_s = hsv.val[1];
					if (hsv.val[2] < min_v)
						min_v = hsv.val[2];

					if (hsv.val[0] > max_h)
						max_h = hsv.val[0];
					if (hsv.val[1] > max_s)
						max_s = hsv.val[1];
					if (hsv.val[2] > max_v)
						max_v = hsv.val[2];
				}
			}
		}

		lower_range = cv::Scalar(min_h, min_s, min_v - tolerance);
		upper_range = cv::Scalar(max_h, max_s, max_v + tolerance);
		std::cout << "Range: " << lower_range << ", " << upper_range
				<< std::endl;
		inRange(imgHSV, lower_range, upper_range, out);
		imshow(window_name4, out);
		while (true) {

			int keyPressed = cv::waitKey(1);
			if (keyPressed == 1048603)
				break;
		}

		destroyWindow(window_name1);
		destroyWindow(window_name2);
		destroyWindow(window_name3);
		destroyWindow(window_name4);

		return true;

	}

	void detect_markers(Mat frame) {
		Mat hsv, mask;
		Mat morpho_kernell = getStructuringElement(MORPH_ELLIPSE, Size(11, 11));

		string window_name1 = "Ranged image";
		string window_name2 = "Image";

		frame = normalize_image(frame);
		cv::cvtColor(frame, hsv, CV_BGR2HLS);
		cv::inRange(hsv, lower_range, upper_range, mask);

		morphologyEx(mask, mask, MORPH_CLOSE, morpho_kernell);

		Mat canny_output;
		vector<vector<Point> > contours;
		vector<Vec4i> hierarchy;

		/// Detect edges using canny
		//Canny(src_gray, canny_output, thresh, thresh * 2, 3);
		/// Find contours
		findContours(mask.clone(), contours, hierarchy, CV_RETR_EXTERNAL,
				CV_CHAIN_APPROX_SIMPLE, Point(0, 0));

		if (contours.size() > 0) {
			vector<Point2f> centers;
			cout << "Contours: " << contours.size() << endl;
			for (int i = 0; i < contours.size(); i++) {
				//vector<Point> cnt = contours[i];
				Point2f center;
				float radius;
				Moments mu;
				minEnclosingCircle((Mat) contours[i], center, radius);
				mu = moments(contours[i], true);
				Point2f center_m(mu.m10 / mu.m00, mu.m01 / mu.m00);

				if (radius > 10) {
					//cout << "Center: " << center << endl;
					//cout << "Center_m: " << center_m << endl << "****" << endl;
					circle(frame, center, radius, Scalar(0, 0, 255), 2,
							LINE_AA);
					circle(frame, center_m, 3, Scalar(0, 255, 255), -1,
							LINE_AA);
					centers.push_back(center_m);
				}
			}

			cout << "Detected markers: " << centers.size() << endl;
		}

		imshow(window_name1, mask);
		imshow(window_name2, frame);

	}

	int get_tolerance() {
		return tolerance;
	}
	void set_tolerance(int tolerance) {
		this->tolerance = tolerance;
	}
	ColorMarkersDetctor() :
			radius(7), click_one(false), click_two(false), tolerance(10) {
		cout << "Working with tolerance: " << tolerance << endl;
	}
	void compute_from_videocap(int camera_id) {
		VideoCapture cap(camera_id); // open the default camera
		if (!cap.isOpened())  // check if we succeeded
			return;
		Mat frame;

		while (true) {
			cap >> frame;
			cout << "Press n to go to next frame" << endl;
			if (calibrate(frame))
				break;
		}
		while (true) {
			cap >> frame;

			detect_markers(frame);
			int keyPressed = cv::waitKey(1);
			if (keyPressed == 1048603)
				break;
		}
	}
private:
	Point point1;
	Point point2;
	int radius;
	bool click_one;
	bool click_two;
	cv::Scalar lower_range, upper_range;
	int tolerance;
};

int main(int argc, char** argv) {
	ColorMarkersDetctor detector;
	if (argc > 1) {
		int val = std::stoi(argv[1]);
		detector.set_tolerance(val);
	}

	detector.compute_from_videocap(1);
	return 0;
}
