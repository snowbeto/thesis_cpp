#include <visualizer.hpp>

bool Visualizer::wasStopped() {
	return myWindow.wasStopped();
}
;

void Visualizer::spinOnce() {

	myWindow.spinOnce(1, true);
}
;

Visualizer::Visualizer() :
		line_width(Configuration::Instance()->get_viz_line_width()), text_size(
				Configuration::Instance()->get_viz_text_size()), spehre_radius(
				Configuration::Instance()->get_viz_sphere_radius()), sphere_resolution(
				10), myWindow("Skeleton") {
	// Add coordinate axes
}
;

void Visualizer::removeAllWidgets() {
	myWindow.removeAllWidgets();
}
;

void Visualizer::close() {
	myWindow.close();
}
;
/*void Visualizer::add_elemnets(const vector<Point3f> &left_points,
 const vector<float> &left_angles, const vector<Point3f>& right_points,
 const vector<float> &right_angles) {
 //add 3D coordinate system
 myWindow.showWidget("Coordinate Widget", viz::WCoordinateSystem(10));
 add_elemnets(left_points, left_angles, "left");
 add_elemnets(right_points, right_angles, "right");

 }
 ;*/

void Visualizer::add_elemnets(const vector<Point3f> &points,
		const vector<float> angles) {
	if (points.size() < 1)
		return;

	myWindow.showWidget("Coordinate Widget", viz::WCoordinateSystem(10));
	myWindow.setWindowSize(Size(640, 360));

	//add lines and spheres
	for (int x = 0; x < points.size() - 1; x++) {
		//cout << "Point " << points[x] << endl;
		//add lines between the curren point and the next one
		viz::WLine line(points[x], points[x + 1]);
		line.setRenderingProperty(viz::LINE_WIDTH, line_width);
		myWindow.showWidget("Line" + to_string(x), line);

		myWindow.showWidget("text3dDist" + to_string(x),
				viz::WText3D(to_string(get_distance(points[x], points[x + 1])),
						(points[x] + points[x + 1]) / 2.0, text_size, false,
						viz::Color::blue()));

		//add spheres at every point
		myWindow.showWidget("sphere" + to_string(x),
				viz::WSphere(points[x], spehre_radius, sphere_resolution,
						viz::Color::red()));

		if (x > 0) {
			Point3f point = points[x];
			point.x += 2;
			point.y += 2;
			myWindow.showWidget("text3d" + to_string(x),
					viz::WText3D(to_string(angles[x - 1]), point, text_size,
							false, viz::Color::green()));
			//cout << "Angle: " << angles[x - 1] << endl;
		}
	}

	//add last sphere
	int last_index = points.size() - 1;
	myWindow.showWidget("sphere" + to_string(last_index),
			viz::WSphere(points[last_index], spehre_radius, sphere_resolution,
					viz::Color::red()));

}
/*int main() {

 Visualizer myWindow;
 vector < Point3f > points;
 points.push_back(Point3f(10, 10, 1));
 points.push_back(Point3f(10, 5, 1));
 points.push_back(Point3f(20, 3, 1));
 points.push_back(Point3f(5, 0, 1));

 // Add line to represent (1,1,1) axis

 cout << "Press q, Q, e, E to quit " << endl;

 while (!myWindow.wasStopped()) {

 //myWindow.setWidgetPose("Cube Widget", pose);
 myWindow.add_elemnets(points);
 myWindow.spinOnce();
 myWindow.removeAllWidgets();
 //myWindow.close();
 }

 return 0;
 }*/
