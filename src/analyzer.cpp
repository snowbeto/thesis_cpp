#include <analyzer.hpp>
#include <algorithm>
#include <configuration.hpp>
#include <cmath>
#include <chrono>
#include <thread>

class SorterY {
public:
	bool operator()(Point3f p1, Point3f p2) {
		return p1.y < p2.y;
	}
} sorterY;

class SorterX {
public:
	bool operator()(Point3f p1, Point3f p2) {
		return p1.x < p2.x;
	}
} sorterX;

/*void Analyzer::compute_angles(vector<Point3f> &points_left,
 vector<float> &angles_left, vector<Point3f> & points_right,
 vector<float> &angles_right) {
 compute_angles(points_left, angles_left);
 compute_angles(points_right, angles_right);

 }
 ;*/

void Analyzer::compute_angles(vector<Point3f> &points, vector<float> &angles) {
	if (points.size() < 3)
		return;

	Point3f p_top, p_middle, p_down;
	Point3f p1_origin = p_top - p_middle;
	Point3f p2_origin = p_down - p_middle;

	//vector<Point3f> translated_points = points;

	/*for (int x = 0; x < points.size(); x++) {
	 Mat point3d(3, 1,
	 CV_64F);
	 Point3f point = points[x];

	 point3d.at<double>(0, 0) = point.x;
	 point3d.at<double>(1, 0) = point.y;
	 point3d.at<double>(2, 0) = point.z;

	 Point3f middle(point), top(point), down(point);

	 top.x = 0;

	 middle.x = 0;
	 middle.z = 0;

	 Point3f p1_origin = down - middle, p2_origin = top - middle;

	 float angle = -acos(
	 p1_origin.dot(p2_origin) / (norm(p1_origin) * norm(p2_origin)));
	 Mat RY =
	 (Mat_<double>(3, 3) << cos(angle), 0, sin(angle), 0, 1, 0, -sin(
	 angle), 0, cos(angle));
	 ;

	 cout << "RY: " << RY << endl;
	 cout << "Point before rotation " << point3d << endl;
	 point3d = RY * point3d;
	 cout << "Point after rotation" << point3d << endl;

	 point.x = point3d.at<double>(0, 0);
	 point.y = point3d.at<double>(1, 0);
	 point.z = point3d.at<double>(2, 0);

	 translated_points.push_back(point);
	 }*/

	for (int x = 0; x < points.size() - 2; x++) {
		p_down = points[x];
		p_middle = points[x + 1];
		p_top = points[x + 2];

		/*p_down = p_down / p_down.x;
		 p_middle = p_middle / p_middle.x;
		 p_top = p_top / p_top.x;*/

		Point3f p1_origin = p_top - p_middle;
		Point3f p2_origin = p_down - p_middle;
		float angle = acos(
				p1_origin.dot(p2_origin) / (norm(p1_origin) * norm(p2_origin)))
				* 180 / M_PI;

		angles.push_back(angle);
	}
}
;

float distance(Point3f p1, Point3f p2) {
	return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2) + pow(p1.z - p2.z, 2));
}

void Analyzer::separate_points(vector<Point3f> & points,
		vector<Point3f> &skeleton_points, bool& valid, vector<int>& counters) {

	valid = true;
	if (points.size() < 1)
		return;

	if (Configuration::Instance()->get_vertical_configuration()) {
		separate_points_bottom_top(points, skeleton_points);

	} else {
		separate_points_right_left(points, skeleton_points);
	}

	//We assume that the 6 markers are the only thing visible in the first frame
	//we initialize the position of the 7 markers representing the body
	if (previous_state.size() == 0) { //init previous state

		if (skeleton_points.size() == 7) {
			previous_state = skeleton_points;
			valid = true;

			for (int x = 0; x < 7; x++) {
				KFmarkers[x].init(previous_state[x]);
			}

			counters[0] = counters[0] + 1;
			counters[1] = counters[1] + 1;
			counters[2] = counters[2] + 1;
			counters[3] = counters[3] + 1;
			counters[4] = counters[4] + 1;
			counters[5] = counters[5] + 1;
			counters[6] = counters[6] + 1;
		}
	}

	else {
		if (skeleton_points.size() == 7) {

			valid = true;

			previous_state[0] = KFmarkers[0].estimate(skeleton_points[0]);
			previous_state[1] = KFmarkers[1].estimate(skeleton_points[1]);
			previous_state[2] = KFmarkers[2].estimate(skeleton_points[2]);
			previous_state[3] = KFmarkers[3].estimate(skeleton_points[3]);
			previous_state[4] = KFmarkers[4].estimate(skeleton_points[4]);
			previous_state[5] = KFmarkers[5].estimate(skeleton_points[5]);
			previous_state[6] = KFmarkers[6].estimate(skeleton_points[6]);

			counters[0] = counters[0] + 1;
			counters[1] = counters[1] + 1;
			counters[2] = counters[2] + 1;
			counters[3] = counters[3] + 1;
			counters[4] = counters[4] + 1;
			counters[5] = counters[5] + 1;
			counters[6] = counters[6] + 1;

		}

		else {/*

		 for (int point_index = 0; point_index < previous_state.size();
		 point_index++) {
		 Point3f old_point = previous_state[point_index];
		 double x = 5.0, x0 = old_point.x, y = 5.0, y0 = old_point.y, z,
		 z0 = old_point.z;
		 double varianceX = 6.0, varianceY = 8.0;
		 double standard_deviation = sqrt(varianceX);

		 double prob = -1.0;
		 double max_dist = 0.0;
		 int index;
		 double current_dist;
		 for (int i = 0; i < skeleton_points.size(); i++) {

		 x = skeleton_points[i].x;
		 y = skeleton_points[i].y;
		 z = skeleton_points[i].z;

		 double p = exp(
		 -((pow(x - x0, 2) / (2 * varianceX))
		 + (pow(y - y0, 2) / (2 * varianceY))
		 + (pow(z - z0, 2) / (2 * varianceY))));

		 if (p > prob) {
		 prob = p;
		 index = i;
		 current_dist = fabs(
		 norm(old_point - skeleton_points[i]));
		 }

		 }

		 if (prob > 0.01 && current_dist < 10.0) {

		 previous_state[point_index] =
		 KFmarkers[point_index].estimate(
		 skeleton_points[index]);
		 skeleton_points.erase(skeleton_points.begin() + index);

		 counters[point_index] = counters[point_index] + 1;

		 } else {

		 //previous_state[point_index] =
		 //		KFmarkers[point_index].predict();
		 }

		 }*/

		}

	}

	skeleton_points = previous_state;

}
;

void Analyzer::separate_points_right_left(vector<Point3f> & points,
		vector<Point3f> &skeleton_points) {
	vector<Point3f> left_points, right_points;
	vector<Point3f> temp1, temp2, temp3, temp4;

	std::sort(points.begin(), points.end(), sorterY);
	left_points.push_back(points[6]);
	points.erase(points.end());

	std::sort(points.begin(), points.end(), sorterX);

	/*temp1.push_back(points[0]);
	 temp1.push_back(points[1]);

	 temp2.push_back(points[2]);
	 temp2.push_back(points[3]);

	 temp3.push_back(points[4]);
	 temp3.push_back(points[5]);

	 temp4.push_back(points[6]);*/

	/*std::sort(temp1.begin(), temp1.end(), sorterX);
	 std::sort(temp2.begin(), temp2.end(), sorterX);
	 std::sort(temp3.begin(), temp3.end(), sorterX);*/

	//get right points
	/*right_points.push_back(temp1[0]);
	 right_points.push_back(temp2[0]);
	 right_points.push_back(temp3[0]);

	 left_points.push_back(temp1[1]);
	 left_points.push_back(temp2[1]);
	 left_points.push_back(temp3[1]);
	 left_points.push_back(temp4[0]);  */

	for (int x = 0; x < points.size(); x++) {
		if (x < 3) {
			right_points.push_back(points[x]);
		} else {
			left_points.push_back(points[x]);
		}
	}
	std::sort(right_points.begin(), right_points.end(), sorterY);
	std::sort(left_points.begin(), left_points.end(), sorterY);

	skeleton_points.insert(skeleton_points.end(), left_points.begin(),
			left_points.end());
	skeleton_points.insert(skeleton_points.end(), right_points.rbegin(),
			right_points.rend());

}
;

void Analyzer::separate_points_bottom_top(vector<Point3f> & points,
		vector<Point3f> &skeleton_points) {
	vector<Point3f> top_points, bottom_points;

	std::sort(points.begin(), points.end(), sorterY);

	for (int x = 0; x < points.size(); x++) {
		if (x < 3) {
			bottom_points.push_back(points[x]);
		} else {
			top_points.push_back(points[x]);
		}
	}

	std::sort(bottom_points.begin(), bottom_points.end(), sorterX);
	std::sort(top_points.begin(), top_points.end(), sorterX);

	skeleton_points.insert(skeleton_points.end(), top_points.begin(),
			top_points.end());
	skeleton_points.insert(skeleton_points.end(), bottom_points.rbegin(),
			bottom_points.rend());

}
;

