#include <configuration.hpp>

Configuration* Configuration::instance = NULL;

Configuration::Configuration() :
		config_dir("./config/"), config_file("config.txt"), counter_filemam(
				"counter.txt"), video_counter_filename("vid_counter.txt"), ranges_file(
				"ranges.xml"), upper_scalar_string("Uppercalar"), lower_scalar_string(
				"LowerScalar"), num_ranges_string("nRanges"), angles_size_variable_name(
				"nAngles"), angles_variable_name("angles_") {
	int argc = 1;
	string n = "./sony.o";
	int arg_limit = 100;
	std::string line;
	string dash = "-";
	vector<string> args(100);

	const char* argv[100];
	argv[0] = n.c_str();

	String keys =
			"{help h usage ? |      | print this message   }"
					"{left_camera_address                  | 192.168.0.102   | ip address of left camera   }"
					"{right_camera_address                 | 192.168.0.101   |  ip address of right camera   }"
					"{camera_port                          | 8080                 | camera port   }"
					"{min_euclidean_dist_between_markers   | 5    | used to group pixels together }"
					"{calibration_tolerance                | 5     | color calibration calibration tolerance      }"
					"{minimum_valid_detected_marker_radius | 10 | minimun valid detected marker radius }"
					"{block_size                           | 5   | blocksize for the stereo matcher    }"
					"{number_of_disparities                |-1  | number of disparities for the stereo matcher}"
					"{wls_filter_lambda                    | 8000  | Lamdba value for the disparity creator}"
					"{wls_filter_sigmaColor                | 2.5  | Sigma value for the disparity creator}"
					"{viz_sphere_radius                    | 1 | Radius of the sphere representing the joints}"
					"{viz_line_width                	   | 4  | Width of the lines between the joints}"
					"{viz_text_size                        | 4 | Size of the text showing the angles}"
					"{max_depth                            | 100 | Maximum valid depth of a 3d point in cms}"
					"{reduction_ratio                      | 0.10 | Reducion ration of the depth image in precentage}"
					"{left_video                           | left01.avi | Reducion ration of the depth image in precentage}"
					"{right_video                          | right01.avi | Reducion ration of the depth image in precentage}"
					"{video_dir                      	   | ./data/videos/ | Directory to read the videos from}"
					"{vertical_configuration               | false | Tells if the cameras are in a vertical configuration}"
					"{fps                      		       | 5 | Use to computed the 3d model based on the specified number of frames}"
					"{use_real_cameras                     | false | use the real cameras to record and take images?}"
					"{calibrate_maker_color                | true | Display the gui to select the color of the markers?}"
					"{vid_list_file | videos_list.xml | files containing the list of videos to train the svn}"
					"{vid_dirs_file | dirs.txt | file containing the list of available dirs}"
					"{tranning_vids_dir | ./data/real_videos/ | main directory of the videos used to train the svn}"
					"{angles_file_extension | _angels.yml | file extension of the files containing the joint angles}"
					"{store_angles | true | flag used to store the angles}"
					"{svn_trainnig_dir | ./data/real_videos/training/ | flag used to store the angles}"
					"{svn_predict_dir | ./data/real_videos/predict/ | flag used to store the angles}"
					"{good_angles_file | good_angles.xml | flag used to store the angles}"
					"{bad_angles_file | bad_angles.xml | flag used to store the angles}"
					"{generate_demo_videos | false | flag used to determine if demo videos should be generated}"
					"{show_output | true | Show outputImages?}"

	;

	cout << "**********Parameters: " << endl;

	std::string::size_type sz;
	std::string::size_type sz2;
	std::ifstream config(config_dir + config_file);

	if (config.is_open()) {
		std::cout << "Reding file: config/config.txt" << std::endl;

		while (getline(config, line)) {

			args[argc] = dash + line;
			argv[argc] = args[argc].c_str();
			argc++;
			if (argc >= arg_limit) {
				cout << "Please increase arg limit" << endl;
				break;
			}
		}
		config.close();
	} else {
		std::cout << "config.txt not found, using default values" << std::endl;
	}

	/*
	 * Read  counter files
	 * */

	//Image counter file
	std::ifstream counter(config_dir + counter_filemam);

	if (counter.is_open()) {
		std::cout << "Reding file: counter.txt" << std::endl;
		while (getline(counter, line)) {
			frame_counter = std::stoi(line, &sz);
		}

		counter.close();
	} else {
		std::cout << "counter.txt not found, setting counter to 100"
				<< std::endl;
		frame_counter = 100;

	}

	//Video counter file
	std::ifstream vid_counter_file(config_dir + video_counter_filename);

	if (vid_counter_file.is_open()) {
		std::cout << "Reding file: vid_counter.txt" << std::endl;
		while (getline(vid_counter_file, line)) {
			cout << line << endl;
			vid_counter = std::stoi(line, &sz2);
		}

		vid_counter_file.close();
	} else {
		std::cout << "vid_counter.txt not found, setting counter to 100"
				<< std::endl;
		vid_counter = 100;

	}

	CommandLineParser parser(argc, argv, keys);

	left_camera_address = parser.get<string>("left_camera_address");
	right_camera_address = parser.get<string>("right_camera_address");
	camera_port = parser.get<string>("camera_port");
	min_euclidean_dist_between_markers = parser.get<int>(
			"min_euclidean_dist_between_markers");
	calibration_tolerance = parser.get<int>("calibration_tolerance");
	minimum_valid_detected_marker_radius = parser.get<int>(
			"minimum_valid_detected_marker_radius");
	block_size = parser.get<int>("block_size");
	number_of_disparities = parser.get<int>("number_of_disparities");
	wls_filter_lambda = parser.get<int>("wls_filter_lambda");
	wls_filter_sigmaColor = parser.get<float>("wls_filter_sigmaColor");
	viz_sphere_radius = parser.get<int>("viz_sphere_radius");
	viz_line_width = parser.get<int>("viz_line_width");
	viz_text_size = parser.get<int>("viz_text_size");
	max_depth = parser.get<float>("max_depth");
	reduction_ratio = parser.get<float>("reduction_ratio");
	right_video = parser.get<string>("right_video");
	left_video = parser.get<string>("left_video");
	video_dir = parser.get<string>("video_dir");
	vertical_configuration = parser.get<bool>("vertical_configuration");
	fps = parser.get<int>("fps");
	use_real_cameras = parser.get<bool>("use_real_cameras");
	calibrate_maker_color = parser.get<bool>("calibrate_maker_color");
	vid_dirs_file = parser.get<string>("vid_dirs_file");
	vid_list_file = parser.get<string>("vid_list_file");
	tranning_vids_dir = parser.get<string>("tranning_vids_dir");
	angles_file_extension = parser.get<string>("angles_file_extension");
	store_angles = parser.get<bool>("store_angles");
	svn_trainnig_dir = parser.get<string>("svn_trainnig_dir");
	svn_predict_dir = parser.get<string>("svn_predict_dir");
	good_angles_file = parser.get<string>("good_angles_file");
	bad_angles_file = parser.get<string>("bad_angles_file");
	generate_demo_videos = parser.get<bool>("generate_demo_videos");
	show_output = parser.get<bool>("show_output");

	cout << "left camera : " << left_camera_address << endl;
	cout << "rigt camera : " << right_camera_address << endl;
	cout << "port : " << camera_port << endl;
	cout << "min euclidean : " << min_euclidean_dist_between_markers << endl;
	cout << "calibration tolerance : " << calibration_tolerance << endl;
	cout << "min val radius : " << minimum_valid_detected_marker_radius << endl;
	cout << "blocksize : " << block_size << endl;
	cout << "disparities : " << number_of_disparities << endl;
	cout << "lamdba : " << wls_filter_lambda << endl;
	cout << "sigma : " << wls_filter_sigmaColor << endl;
	cout << "viz_sphere_radius : " << viz_sphere_radius << endl;
	cout << "viz_line_width : " << viz_line_width << endl;
	cout << "viz_text_size : " << viz_text_size << endl;
	cout << "max_depth: " << max_depth << endl;
	cout << "reduction_ratio: " << reduction_ratio << endl;
	cout << "left vid: " << left_video << endl;
	cout << "right_vid: " << right_video << endl;
	cout << "video_dir: " << video_dir << endl;
	cout << "vertical_configuration: " << vertical_configuration << endl;
	cout << "FPS: " << fps << endl;

	cout << "use_real_cameras: " << use_real_cameras << endl;
	cout << "Counter: " << frame_counter << endl;
	cout << "Vid counter: " << vid_counter << endl;
	cout << "Ranges size: " << upper_ranges.size() << endl;
	cout << "calibrate_maker_color: " << calibrate_maker_color << endl;
	cout << "Videos directories: " << vid_dirs_file << endl;
	cout << "Videos list file: " << vid_list_file << endl;
	cout << "Training vids dir: " << tranning_vids_dir << endl;
	cout << "Angles file extension: " << angles_file_extension << endl;
	cout << "Store angles: " << store_angles << endl;
	cout << "SVN Training dir: " << svn_predict_dir << endl;
	cout << "SVN Predict dir: " << svn_trainnig_dir << endl;
	cout << "Good angles filename: " << good_angles_file << endl;
	cout << "Bad angles filename: " << bad_angles_file << endl;
	cout << "Save demo videos: " << generate_demo_videos << endl;
	cout << "Show output: " << show_output << endl;
	cout << "**********" << endl;
}

void Configuration::update_frame_counter_file(int& val) {
	std::ofstream counter_file;
	counter_file.open(config_dir + counter_filemam);
	counter_file << val;
	counter_file.close();

}

void Configuration::update_vid_counter_file(int& val) {
	std::ofstream video_counter_file;
	video_counter_file.open(config_dir + video_counter_filename);
	video_counter_file << val;
	video_counter_file.close();

}

void Configuration::save_ranges(vector<Scalar> &upper_ranges,
		vector<Scalar> &lower_ranges) {
	if (upper_ranges.size() < 1)
		return;

	FileStorage fs(config_dir + ranges_file, FileStorage::WRITE);

	for (int i = 0; i < upper_ranges.size(); i++) {
		fs << upper_scalar_string + to_string(i) << upper_ranges[i];
		fs << lower_scalar_string + to_string(i) << lower_ranges[i];
	}

	int size = upper_ranges.size();
	fs << num_ranges_string << size;

	fs.release();
}

void Configuration::save_angles(string dir, string file_name,
		vector<vector<float>>& angles_complete) {
	Mat angles_matrix(1, 5, CV_32F);
	cout << "saving angles: " << angles_complete.size() << endl;

	file_name.erase(file_name.size() - 4, 4);

	FileStorage fs(dir + "data/" + file_name + angles_file_extension,
			FileStorage::WRITE);

	for (int x = 0; x < angles_complete.size(); x++) {
		vector<float> array = angles_complete[x];
		for (int y = 0; y < array.size(); y++) {
			angles_matrix.at<float>(0, y) = array[y];
		}
		fs << angles_variable_name + to_string(x) << angles_matrix;
	}
	int size = angles_complete.size();
	fs << angles_size_variable_name << size;

	fs.release();

}

