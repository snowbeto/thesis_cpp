#include <stereo_disparity_creator.hpp>
#include <color_markers_detector.hpp>
#include <visualizer.hpp>
#include <analyzer.hpp>
#include <sony_camera.hpp>
#include <human_detector.hpp>
#include <configuration.hpp>
#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>
#include <camera_parameters.hpp>
#include <chrono>
#include <thread>
#include <opencv2/ml.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/plot.hpp>
#include <fstream>

vector<string> class_results;
int class_results_counter = 0;

enum TraininType {
	historgrams = 0, low_pass_filter = 1
};
void init() {
	Configuration::Instance();
	CameraParameters::Instance();
}
void create_csv(vector<float> values, string file_name) {
	ofstream myfile;
	myfile.open(file_name.c_str());
	for (int x = 0; x < values.size(); x++) {
		myfile << values[x];
		myfile << "\n";
	}
	myfile.close();

}
void create_xml(vector<string> &left_images, vector<string> &right_images) {
	FILE* fp = fopen("./stereo_calib.xml", "wt");
	fprintf(fp, "<?xml version=\"1.0\"?>\n");
	fprintf(fp, "<opencv_storage>\n");
	fprintf(fp, "<imagelist>\n");

	for (int y = 0; y < left_images.size(); y++) {
		fprintf(fp, "\"%s\"\n", left_images[y].c_str());
		fprintf(fp, "\"%s\"\n", right_images[y].c_str());
	}
	fprintf(fp, "</imagelist>\n");
	fprintf(fp, "</opencv_storage>\n");
	fclose(fp);
}

void merge_left_right_frames(Mat& l, Mat& r, Mat & combined_frame) {

	Size sz1 = l.size();
	Size sz2 = r.size();
	combined_frame = Mat(sz1.height, sz1.width + sz2.width, l.type());
	l.copyTo(combined_frame(Rect(0, 0, sz1.width, sz1.height)));
	r.copyTo(combined_frame(Rect(sz1.width, 0, sz2.width, sz2.height)));

}

void merge_top_down_frames(Mat& t, Mat& d, Mat & combined_frame) {
	Size sz1 = t.size();
	Size sz2 = d.size();
	combined_frame = Mat(sz1.height + sz2.height, sz1.width, t.type());
	t.copyTo(combined_frame(Rect(0, 0, sz1.width, sz1.height)));
	d.copyTo(combined_frame(Rect(0, sz1.height, sz2.width, sz2.height)));
}
void get_live_streams(SACamera& camera_left, SACamera& camera_right,
		SAMultiCameraHandler &handler) {
	std::string windowName1 = "Left";
	std::string windowName2 = "Right";
	StereoDisparityCreator disparityCreator;

	cout << "Press r to record, press s to stop recording" << endl;
	vector<Mat> buffer_left;
	vector<Mat> buffer_right;

	buffer_left.reserve(20000);
	buffer_right.reserve(20000);

	VideoWriter left_writer;
	VideoWriter right_writer;
	HumanDetector aHumanDetector;
	int vid_counter = Configuration::Instance()->get_vid_counter();

	bool RECORD = false;
	int counter = 0;
	while (true) {
		// r 1048690
		// s
		handler.read();
		Mat left, right, left_camera, right_camera;
		if (camera_left.can_read_image() && camera_right.can_read_image()) {
			left_camera = left = camera_left.get_image();
			right_camera = right = camera_right.get_image();

			disparityCreator.undistort_left_image_lp(left);
			disparityCreator.undistrort_right_image_lp(right);

			Mat combined_frame;
			merge_left_right_frames(left, right, combined_frame);

			cv::imshow(windowName1, combined_frame);
			//cv::imshow(windowName2, right);

			if (RECORD) {

				if (counter == 3) {
					buffer_left.push_back(left_camera);
					buffer_right.push_back(right_camera);
					counter = 0;
				} else {
					counter++;
				}
			}
		}

		int keyPressed = cv::waitKey(1);

		if (keyPressed != -1)
			cout << keyPressed << endl;
		if (keyPressed == 1048603) {
			if (RECORD) {
				cout << "Press s to stop recording" << endl;
			} else

			{
				camera_left.data.read = false;
				camera_right.data.read = false;
				break;
			}
		}

		if (keyPressed == 1048690) {		//r
			if (RECORD) {
				cout << "Press s to stop recording" << endl;
			} else {
				RECORD = true;

				//send the command to the real cameras
				if (Configuration::Instance()->get_use_real_cameras())
					handler.start_recording();

				cout << "Recording videos" << endl;

				left_writer.open(
						"data/videos/left" + to_string(vid_counter) + ".avi",
						cv::VideoWriter::fourcc('X', 'V', 'I', 'D'), 30,
						left.size());
				right_writer.open(
						"data/videos/right" + to_string(vid_counter) + ".avi",
						cv::VideoWriter::fourcc('X', 'V', 'I', 'D'), 30,
						right.size());

				vid_counter++;
				Configuration::Instance()->update_vid_counter_file(vid_counter);
			}
		}
		if (keyPressed == 1048691) {		//s
			if (RECORD) {

				RECORD = false;

				//send the command to the real camera
				if (Configuration::Instance()->get_use_real_cameras())
					handler.stop_recording();

				cout << "Saving videos. " << endl << "left size: "
						<< buffer_left.size() << " right size: "
						<< buffer_right.size() << endl;

				for (int x = 0; x < buffer_left.size(); x++) {
					left_writer << buffer_left[x];
					right_writer << buffer_right[x];
				}

				buffer_left.clear();
				buffer_right.clear();
				buffer_left.reserve(20000);
				buffer_right.reserve(20000);

				left_writer.release();
				right_writer.release();

			} else {
				cout << "Press r to start recoding " << endl;
			}

		}
	}
	destroyWindow(windowName1);
	//destroyWindow(windowName2);
}

void take_calibration_images(SACamera& camera_left, SACamera& camera_right,
		SAMultiCameraHandler &handler) {
	std::string windowName1 = "Left";
	std::string windowName2 = "Right";
	vector<string> left_images;
	vector<string> right_images;
	int image_counter = Configuration::Instance()->get_frame_counter();
	int counter = 1;
	while (true) {
		handler.read();

		if (camera_left.can_read_image() && camera_right.can_read_image()) {

			cv::imshow(windowName1, camera_left.get_image());
			cv::imshow(windowName2, camera_right.get_image());
		}
		int keyPressed = cv::waitKey(1);
		if (keyPressed == 1048603) {
			camera_left.data.read = false;
			camera_right.data.read = false;
			break;
		} else if (keyPressed == 1048681) { //i
			//save images
			std::string file_number = std::to_string(image_counter);

			if (image_counter < 10) {
				file_number = "0" + file_number;
			}
			//handler.take_images();
			string dir = "./data/calib_images/";
			string left_image = "left" + file_number + ".jpg";
			string right_image = "right" + file_number + ".jpg";
			imwrite(dir + left_image, camera_left.get_image());
			imwrite(dir + right_image, camera_right.get_image());

			image_counter++;
			Configuration::Instance()->update_frame_counter_file(image_counter);

			left_images.push_back(left_image);
			right_images.push_back(right_image);

			if (Configuration::Instance()->get_use_real_cameras()) {
				//record image from camera
				handler.start_recording();
				std::this_thread::sleep_for(std::chrono::milliseconds(1500));
				handler.stop_recording();
			}

			std::cout << endl << "*Image " << counter << " has been saved"
					<< std::endl;
			counter++;

		}
	}
	destroyWindow(windowName1);
	destroyWindow(windowName2);
	create_xml(left_images, right_images);
}

void stereo_match() {
	VideoCapture left(
			Configuration::Instance()->get_video_dir()
					+ Configuration::Instance()->get_left_video());
	VideoCapture right(
			Configuration::Instance()->get_video_dir()
					+ Configuration::Instance()->get_right_video());
	std::string windowName1 = "Left";
	std::string windowName2 = "Right";
	std::string windowName3 = "Disparity map";
	StereoDisparityCreator stereo_matcher;
	Mat l, r;
	while (true) {
		left >> l;
		right >> r;

		if (l.empty() || r.empty())
			break;

		cv::imshow(windowName1, l);
		cv::imshow(windowName2, r);
		Mat disp = stereo_matcher.get_disparity_map(l, r);
		cv::imshow(windowName3, disp);

		int keyPressed = cv::waitKey(1);
		if (keyPressed == 1048603) {
			break;
		}
	}
	destroyWindow(windowName1);
	destroyWindow(windowName2);
	destroyWindow(windowName3);

}
void calibrate_color(SACamera& camera_left, SACamera& camera_right,
		SAMultiCameraHandler &handler, ColorMarkersDetctor& detector) {
	cout
			<< "Please select calibration image. Press enter to select. Press n to get the next image"
			<< endl;
	while (true) {
		handler.read();

		if (camera_left.can_read_image() && camera_right.can_read_image()) {

			if (!detector.calibrate(camera_left.get_image(),
					camera_right.get_image()))
				break;
		}
	}
}

void calibrate_color_vid(VideoCapture vid_left, VideoCapture &vid_right,
		ColorMarkersDetctor& detector) {
	Mat left, right;

	vid_left >> left;
	vid_right >> right;

	while (true) {
		cout
				<< "Please select calibration image. Press enter to select image. Press n to get the next image. Press esc to quit"
				<< endl;
		if (!detector.calibrate(left, right))
			break;
		else {
			vid_left >> left;
			vid_right >> right;
		}
	}

}
void detect_markers(SACamera& camera_left, SACamera& camera_right,
		SAMultiCameraHandler &handler) {
	std::string windowName1 = "Left";
	std::string windowName2 = "Right";
	std::string windowName3 = "Detected markers";
	std::string windowName4 = "Mask A";
	std::string windowName5 = "Mask B";
	std::string windowName6 = "Mask C";
	ColorMarkersDetctor detector("./");
	StereoDisparityCreator stereo_matcher;
	calibrate_color(camera_left, camera_right, handler, detector);
	while (true) {
		handler.read();

		if (camera_left.can_read_image() && camera_right.can_read_image()) {
			cv::Mat left = camera_left.get_image();
			cv::Mat right = camera_right.get_image();
			stereo_matcher.undistort_left_image(left);
			stereo_matcher.undistrort_right_image(right);
			stereo_matcher.match_right__to_left(right);
			cv::Mat mask_a, mask_b, mask_c;
			cv::imshow(windowName1, left);
			cv::imshow(windowName2, right);
			detector.detect_markers_combined(left, right, mask_a, mask_b,
					mask_c, mask_c);
			cv::imshow(windowName3, left);
			cv::imshow(windowName4, mask_a);
			cv::imshow(windowName5, mask_b);
			cv::imshow(windowName6, mask_c);
		}
		int keyPressed = cv::waitKey(1);
		if (keyPressed == 1048603) {
			camera_left.data.read = false;
			camera_right.data.read = false;
			break;
		}
	}
	destroyWindow(windowName1);
	destroyWindow(windowName2);
	destroyWindow(windowName3);
	destroyWindow(windowName4);
	destroyWindow(windowName5);
	destroyWindow(windowName6);

}

void resize_image(Mat& image) {
	resize(image, image, CameraParameters::Instance()->get_resize_size());
}

void stereo_and_marker_detection_vidcap(VideoCapture &vid_left,
		VideoCapture &vid_right, bool calibrate_flag, string current_dir,
		string file_name) {
	std::string windowName1 = "Left-Right";
	std::string windowName2 = "Left-(H)Right";
	std::string windowName3 = "SGBM Detected markers";
	std::string windowName4 = "Mask on left image";
	std::string windowName5 = "Mask combined";
	std::string windowName6 = "SGBM Disparity map";
	std::string windowName7 = "Mask on right image";
	std::string windowName8 = "Blended normal";
	std::string windowName9 = "Blended(H)";

	std::string windowName10 = "merged_image";
	string text = class_results[class_results_counter];
	class_results_counter++;

	string video_name = file_name.erase(file_name.size() - 4, file_name.size())
			+ "_result_video";

	cout << current_dir << " " << video_name << endl;
	VideoWriter writer(current_dir + video_name + ".avi",
			cv::VideoWriter::fourcc('X', 'V', 'I', 'D'), 20, Size(800, 600));

	ColorMarkersDetctor detector(current_dir);
	StereoDisparityCreator stereo_matcher;
	Visualizer myVis;
	HumanDetector aHumanDetector;
	Analyzer anAnalyzer;
	vector<vector<float> > angles_all;
	vector<float> angles_for_values;
	int counter = 0;

	vector<int> counters(7, 0);
	vector<int> final_counters;

	int left_frames = vid_left.get(cv::CAP_PROP_FRAME_COUNT);
	int right_frames = vid_right.get(cv::CAP_PROP_FRAME_COUNT);

	int number_of_frames = left_frames;

	if (right_frames < number_of_frames)
		number_of_frames = right_frames;

	if (!vid_left.isOpened()) {
		cout << "Could not open left video" << endl;
		return;
	}

	if (!vid_right.isOpened()) {
		cout << "Could not open right video" << endl;
		return;
	}

	if (calibrate_flag)
		calibrate_color_vid(vid_left, vid_right, detector);
	else
		detector.init_lower_upper_ranges();

	Mat left, right;
	int count = Configuration::Instance()->get_fps();
	int frame_conter = 0;

	while (true) {
		vid_left >> left;
		vid_right >> right;

		if (left.empty())
			break;

		if (right.empty())
			break;

		//if (count == Configuration::Instance()->get_fps()) {
		//count = 0;
		vector<Point3f> markers;
		vector<Point3f> markers_bm;
		cv::Mat mask_a, mask_b, mask_c;
		cv::Mat disp = stereo_matcher.get_disparity_map(left, right);
		//cv::Mat dist_bm = stereo_matcher.get_disparity_map_bm(left, right);

		stereo_matcher.undistort_left_image(left);
		stereo_matcher.undistrort_right_image(right);
		Mat normal_right = right.clone();
		Mat human = left.clone();

		stereo_matcher.match_right__to_left(right);
		vector<Point> centers = detector.detect_markers_combined(left, right,
				mask_a, mask_b, mask_c, disp);

		markers = stereo_matcher.get_point_depth(disp, left, centers);

		//stereo_matcher.get_point_depth(dist, left, centers);

		vector<Point3f> skeleton_points;
		vector<float> angles;

		bool valid;

		anAnalyzer.separate_points(markers, skeleton_points, valid, counters);
		anAnalyzer.compute_angles(skeleton_points, angles);
		myVis.add_elemnets(skeleton_points, angles);

		if (frame_conter < 300) {
			counter = counter + markers.size();
			final_counters = counters;

		} else {
			break;
		}
		frame_conter++;
		//cout << frame_conter << endl;
		if (angles.size() > 0 && valid) {
			angles_all.push_back(angles);

			/*Mat temp(1, 7, CV_32F);
			 for (int y = 0; y < skeleton_points.size(); y++) {
			 temp.at<float>(0, y) = norm(skeleton_points[y]);
			 }*/
			angles_for_values.push_back(angles[2]);
		}
		myVis.spinOnce();

		Mat graframe;
		//cvtColor(human, graframe, COLOR_RGB2GRAY);
		//aHumanDetector.detect_loweboddy(human, graframe);
		if (Configuration::Instance()->get_use_real_cameras()) {
			resize_image(left);
			resize_image(right);
			resize_image(mask_a);
			resize_image(mask_b);
			resize_image(disp);
			resize_image(mask_c);

		}

		Mat normal_combined, combined, blended, normal_blended;

		merge_left_right_frames(human, right, combined);

		//merge_left_right_frames(human, normal_right, normal_combined);

		/*addWeighted( human, 0.75, right, 0.5, 0.0, blended);
		 addWeighted( human, 0.75, normal_right, 0.5, 0.0, normal_blended);*/

		//if (Configuration::Instance()->get_show_output())
		cv::imshow(windowName1, combined);
		//cv::imshow(windowName2, normal_combined);
		//cv::imshow(windowName3, left);
		//cv::imshow(windowName4, mask_a);
		//cv::imshow(windowName5, mask_b);
		//cv::imshow(windowName6, disp);
		//cv::imshow(windowName7, mask_c);
		//cv::imshow(windowName8, normal_blended);
		//cv::imshow(windowName9, blended);

		myVis.spinOnce();

		if (Configuration::Instance()->get_generate_demo_videos()) {

			Mat viz_image = myVis.get_frame();

			Mat combined_image_top, combined_image_bottom, result;
			myVis.reset_camera();
			if (viz_image.size()
					!= CameraParameters::Instance()->get_resize_size()) {
				myVis.reset_camera();
				resize_image(viz_image);
			}
			merge_left_right_frames(left, viz_image, combined_image_top);

			merge_left_right_frames(disp, mask_b, combined_image_bottom);

			cvtColor(combined_image_bottom, combined_image_bottom, CV_GRAY2BGR);

			merge_top_down_frames(combined_image_bottom, combined_image_top,
					result);

			resize(result, result, Size(800, 600));
			cv::putText(result, text, Point(30, 30), FONT_HERSHEY_PLAIN, 1.7,
					Scalar::all(255), 0.8, 3);
			writer << result;

			if (Configuration::Instance()->get_show_output())
				cv::imshow(windowName10, result);
		}

		/*while (true) {
		 myVis.spinOnce();
		 int keyPressed = cv::waitKey(1);
		 if (keyPressed == 1048686 || keyPressed == 1048603)
		 break;

		 }*/
		myVis.removeAllWidgets();
		//}
		count++;

		int keyPressed = cv::waitKey(1);
		if (keyPressed == 1048603) {
			myVis.close();
			writer.release();
			break;
		}
	}

	cout << Configuration::Instance()->get_store_angles() << endl;
	if (Configuration::Instance()->get_store_angles()) {
		Configuration::Instance()->save_angles(current_dir, file_name,
				angles_all);
	}

	cout << "*****Number of detected Markers: " << counter << endl;
	cout << "**inv counters: ";
	for (int i = 0; i < counters.size(); i++) {
		cout << counters[i] << " ";
	}
	cout << endl;
	//create_csv(angles_for_values, "my_values.csv");
	/*Scalar roi_mean, roi_stdev;
	 for (int i = 0; i < angles_for_values.cols; i++) {

	 meanStdDev(angles_for_values.col(i).rowRange(0, angles_for_values.rows),
	 roi_mean, roi_stdev);

	 cout << roi_stdev[0] << " ";
	 }
	 cout << endl;*/
	//if (Configuration::Instance()->get_show_output())
	destroyWindow(windowName1);
	//destroyWindow(windowName2);
	//destroyWindow(windowName3);
	//destroyWindow(windowName4);
	//destroyWindow(windowName5);
	//destroyWindow(windowName6);
	//destroyWindow(windowName7);
	if (Configuration::Instance()->get_generate_demo_videos()
			&& Configuration::Instance()->get_show_output()) {
		destroyWindow(windowName10);
	}
	//destroyWindow(windowName8);
	//destroyWindow(windowName9);

}

void stereo_and_marker_detection(SACamera& camera_left, SACamera& camera_right,
		SAMultiCameraHandler &handler) {
	std::string windowName1 = "Left";
	std::string windowName2 = "Right";
	std::string windowName3 = "Detected markers";
	std::string windowName4 = "Mask A (Combined a + b)";
	std::string windowName5 = "Mask B";
	std::string windowName7 = "Mask C (On right image)";
	std::string windowName6 = "Disparity map";
	StereoDisparityCreator stereo_matcher;
	Visualizer myVis;

	ColorMarkersDetctor detector("./");

	calibrate_color(camera_left, camera_right, handler, detector);
	while (true) {
		handler.read();
		vector<Point3f> markers;

		if (camera_left.can_read_image() && camera_right.can_read_image()) {
			cv::Mat left = camera_left.get_image();
			cv::Mat right = camera_right.get_image();
			cv::Mat mask_a, mask_b, mask_c;
			cv::Mat disp = stereo_matcher.get_disparity_map(left, right);

			cv::imshow(windowName1, left);
			cv::imshow(windowName2, right);
			stereo_matcher.undistort_left_image(left);
			stereo_matcher.undistrort_right_image(right);
			stereo_matcher.match_right__to_left(right);
			vector<Point> centers = detector.detect_markers_combined(left,
					right, mask_a, mask_b, mask_c, disp);

			markers = stereo_matcher.get_point_depth(disp, left, centers);

			//myVis.add_elemnets(markers);
			//myVis.spinOnce();
			cv::imshow(windowName3, left);
			cv::imshow(windowName4, mask_a);
			cv::imshow(windowName5, mask_b);
			cv::imshow(windowName6, disp);
			cv::imshow(windowName7, mask_c);
			myVis.removeAllWidgets();
		}
		int keyPressed = cv::waitKey(1);

		while (true) {
			keyPressed = cv::waitKey(1);
			if (keyPressed == 1048686 || keyPressed == 1048603)
				break;

		}

		if (keyPressed == 1048603) {
			camera_left.data.read = false;
			camera_right.data.read = false;
			myVis.close();
			break;
		}
	}
	destroyWindow(windowName1);
	destroyWindow(windowName2);
	destroyWindow(windowName3);
	destroyWindow(windowName4);
	destroyWindow(windowName5);
	destroyWindow(windowName6);
	destroyWindow(windowName7);

}

bool readXMLStringList(const string& filename, vector<string>& list) {
	list.resize(0);
	FileStorage fs(filename, FileStorage::READ);
	if (!fs.isOpened())
		return false;

	FileNode n = fs.getFirstTopLevelNode();
	if (n.type() != FileNode::SEQ)
		return false;

	FileNodeIterator it = n.begin(), it_end = n.end();
	for (; it != it_end; ++it)
		list.push_back((string) *it);
	return true;
}

void reproduce_videos() {
	StereoDisparityCreator stereo_matcher;
	string windowName1 = "Videos";
	string windowName2 = "Disp Map";
	vector<string> video_dirs_list;

	video_dirs_list.push_back(Configuration::Instance()->get_svn_train_dir());
	//video_dirs_list.push_back(Configuration::Instance()->get_svn_predict_dir());

	for (int index = 0; index < video_dirs_list.size(); index++) {

		cout << video_dirs_list[index] << endl;
		std::ifstream vid_dirs(
				video_dirs_list[index]
						+ Configuration::Instance()->get_vid_dirs_file());
		std::string dir, subdir, video;

		/*open subject dir*/
		while (std::getline(vid_dirs, dir)) {
			string trannig_dir = video_dirs_list[index] + dir + "/";
			vector<string> video_list;
			string vid_file = trannig_dir
					+ Configuration::Instance()->get_vid_list_file();
			cout << "Reading files from: " << trannig_dir << endl;

			readXMLStringList(vid_file, video_list);
			int num_vids = (int) video_list.size() / 2; // temporary fix to deal with different directories

			bool calibrate_flag =
					Configuration::Instance()->get_calibrate_maker_color();

			for (int i = 0; i < num_vids; i++) {
				string left_video_name = trannig_dir + video_list[i * 2];
				string right_video_name = trannig_dir + video_list[i * 2 + 1];

				VideoCapture left_video(left_video_name);
				VideoCapture right_video(right_video_name);
				int left_frames = left_video.get(cv::CAP_PROP_FRAME_COUNT);
				int right_frames = right_video.get(cv::CAP_PROP_FRAME_COUNT);

				stereo_and_marker_detection_vidcap(left_video, right_video,
						calibrate_flag, trannig_dir, video_list[i * 2]);

			}

		}
	}

	destroyWindow(windowName1);
	destroyWindow(windowName2);
}

/*All code is based on openCV histogram example*/
void calculate_histogram(std::string file_name, Mat& data, cv::Mat &hist) {

	int histSize = 180;
	float range[] = { 0, 181 };
	const float* histRange = { range };

	bool uniform = true;
	bool accumulate = false;
	int hist_w = 500;
	int hist_h = 500;
	int bin_w = cvRound((double) hist_w / histSize);
	cv::Mat histImage1(hist_h, hist_w, CV_8UC3, cv::Scalar(0, 0, 0));

	/*Create histogram of optical flows*/
	//cv::normalize(flows_matrix, flows_matrix, 0, 500, cv::NORM_MINMAX,
	//CV_32F);
	cv::calcHist(&data, 1, 0, cv::Mat(), hist, 1, &histSize, &histRange,
			uniform, accumulate);

	/*Save histogram into a file*/
	cv::normalize(hist, hist, 0, histImage1.rows, cv::NORM_MINMAX, -1,
			cv::Mat());
	for (int i = 1; i < histSize; i++) {
		cv::line(histImage1,
				cv::Point(bin_w * (i - 1),
						hist_h - cvRound(hist.at<float>(i - 1))),
				cv::Point(bin_w * (i), hist_h - cvRound(hist.at<float>(i))),
				cv::Scalar(255, 0, 0), 2, 8, 0);
	}

	hist = hist.t();
	cv::imwrite(file_name, histImage1);

}

void read_angles(string dirs_path, string angles_file, Mat& angles) {

	std::ifstream angles_dir(
			dirs_path + Configuration::Instance()->get_vid_dirs_file());
	std::string dir, subdir;

	/*open subject dir*/
	while (std::getline(angles_dir, dir)) {
		string complete_dir = dirs_path + dir + "/data/";
		string complete_angles_file = complete_dir + angles_file;
		cout << "Reading files from: " << complete_angles_file << endl;
		vector<string> file_list;

		readXMLStringList(complete_angles_file, file_list);
		for (int x = 0; x < file_list.size(); x++) {
			Scalar scalar;
			int nAngles;

			cout << file_list[x] << endl;
			FileStorage fs2(complete_dir + file_list[x], FileStorage::READ);

			fs2[Configuration::Instance()->get_angles_size_variable_name()]
					>> nAngles;

			Mat angles_in_file;
			for (int i = 0; i < nAngles; ++i) {

				Mat temp;
				fs2[Configuration::Instance()->get_angles_variable_name()
						+ to_string(i)] >> temp;
				angles_in_file.push_back(temp);

			}
			fs2.release();
			Mat hist;

			string FileName = complete_angles_file;
			FileName.erase(FileName.size() - 4, FileName.size());
			string file = file_list[x];
			file.erase(file.size() - 4, file.size());
			FileName = FileName + "_" + file + "_hist.jpg";
			//cout << FileName << endl;
			calculate_histogram(FileName, angles_in_file, hist);
			angles.push_back(hist);
		}

	}
	cout << angles.rows << endl;
}

void read_angles_belly(string dirs_path, string angles_file, Mat& angles) {

	std::ifstream angles_dir(
			dirs_path + Configuration::Instance()->get_vid_dirs_file());
	std::string dir, subdir;

	/*open subject dir*/
	while (std::getline(angles_dir, dir)) {
		string complete_dir = dirs_path + dir + "/data/";
		string complete_angles_file = complete_dir + angles_file;
		cout << "Reading files from: " << complete_angles_file << endl;
		vector<string> file_list;

		readXMLStringList(complete_angles_file, file_list);
		for (int x = 0; x < file_list.size(); x++) {
			Scalar scalar;
			int nAngles;

			cout << file_list[x] << endl;
			FileStorage fs2(complete_dir + file_list[x], FileStorage::READ);

			fs2[Configuration::Instance()->get_angles_size_variable_name()]
					>> nAngles;

			for (int i = 0; i < nAngles; ++i) {

				Mat temp;
				float value[3];
				fs2[Configuration::Instance()->get_angles_variable_name()
						+ to_string(i)] >> temp;
				value[2] = temp.at<float>(0, 2);
				value[1] = temp.at<float>(0, 1);
				value[0] = temp.at<float>(0, 0);

				Mat val(1, 3, CV_32FC1, value);
				angles.push_back(val);

			}
			fs2.release();

		}

	}
	cout << angles.rows << endl;
}

bool sorf(float i, float j) {
	return (i < j);
}

void plot_data(Mat &data, Mat &result) {
	Mat plot_data;

	for (int x = 0; x < data.rows; x++) {
		double values[1];
		values[0] = data.at<float>(x, 0);

		Mat temp(1, 1, CV_64F, values);
		plot_data.push_back(temp);
		//cout << data.at<float>(x, 0) << ": " << plot_data.at<double>(x, 0)
		//		<< endl;
	}
	Ptr<plot::Plot2d> plot;
	plot = plot::Plot2d::create(plot_data);
	Scalar bk(200, 200, 200);
	plot->setPlotBackgroundColor(bk);
	plot->setMaxY(190.0);
	plot->setMinY(0.0);
	plot->render(result);

}

void create_csv(Mat normal_data, Mat filtered_data, string file_name) {

	for (int i = 0; i < normal_data.cols; i++) {
		ofstream myfile;
		string name = file_name + to_string(i) + ".csv";
		myfile.open(name.c_str());
		myfile << "Normal data,Filtered data\n";
		for (int j = 0; j < normal_data.rows; j++) {
			myfile << normal_data.at<float>(j, i) << ","
					<< filtered_data.at<float>(j, i) << "\n";

		}

		myfile.close();

	}
}
void filter_data(Mat& data) {

	float beta = 0.05;

	Mat filetered_data = data.clone();
	for (int x = 1; x < data.rows; x++) {
		filetered_data.at<float>(x, 0) = (beta * data.at<float>(x, 0))
				+ ((1 - beta) * filetered_data.at<float>(x - 1, 0));

		filetered_data.at<float>(x, 1) = (beta * data.at<float>(x, 1))
				+ ((1 - beta) * filetered_data.at<float>(x - 1, 1));

		filetered_data.at<float>(x, 2) = (beta * data.at<float>(x, 2))
				+ ((1 - beta) * filetered_data.at<float>(x - 1, 2));

		filetered_data.at<float>(x, 3) = (beta * data.at<float>(x, 3))
				+ ((1 - beta) * filetered_data.at<float>(x - 1, 3));

		filetered_data.at<float>(x, 4) = (beta * data.at<float>(x, 4))
				+ ((1 - beta) * filetered_data.at<float>(x - 1, 4));
	}

	data = filetered_data;
}

void get_gait_cycles(Mat &data, Mat & final_data, string filename) {

	bool up = false;
	bool down = false;
	int starting_row;
	int ending_row;

	Mat normal_nata = data.clone();
	//plot_data(data, res1);
	filter_data(data);

	for (int x = 0; x < data.rows; x++) {

		if (!up) {

			float val = data.at<float>(x, 0);
			int result = 1;
			for (int y = x + 1; y <= x + 2; y++) {
				if (y < data.rows) {
					float diff = val - data.at<float>(y, 0);

					if (diff < 0)
						result *= 1;
					else
						result *= 0;
				}
			}
			if (result == 1) {
				//data.at<float>(x, 0) = 0.0;
				starting_row = x;
				//cout << "From: " << val;
				up = true;
			}

		} else {

			float val = data.at<float>(x, 0);
			int result = 1;
			//cout << "********" << endl;
			for (int y = x + 1; y <= x + 2; y++) {
				if (y < data.rows) {
					float diff = val - data.at<float>(y, 0);
					//cout << diff << endl;
					if (diff > 0)
						result *= 1;
					else
						result *= 0;
				}
			}
			if (result == 1) {
				//data.at<float>(x, 0) = 200.0;
				ending_row = x;
				//cout << " to: " << val << "time:  "
				//		<< ((float)ending_row - (float)starting_row) / 30.0 << endl;
				up = false;
				Mat cycle = data(
						Rect(0, starting_row, 5,
								(ending_row - starting_row) + 1));

				//float values[2];
				//reduce(cycle, row_mean, 0, CV_REDUCE_AVG);
				//values[0] = norm(cycle);
				//values[1] = cycle.rows / 30.0;

				/*Mat angles;
				 for(int x= 0; x < cycle.rows;x++)
				 {
				 float values[4];
				 values[0] = cycle.at<float>(x, 0);
				 values[1] = cycle.at<float>(x, 1);
				 values[2] = cycle.at<float>(x, 3);
				 values[3] = cycle.at<float>(x, 4);
				 Mat temp(1,4, cycle.type(), values);

				 angles.push_back(temp);

				 }*/

				Mat row_mean;				//(1, 2, data.type(), values);
				reduce(cycle, row_mean, 0, CV_REDUCE_AVG);
				final_data.push_back(row_mean);

			}
		}

	}
	filename.erase(filename.size() - 4, filename.size());
	create_csv(normal_nata, data, filename);
	//plot_data(data, res2);
	//merge_left_right_frames(res1, res2, result);
	//imshow("plot", result);
	//waitKey();

}

void read_angles_low_pass(string dirs_path, string angles_file, Mat& angles) {

	std::ifstream angles_dir(
			dirs_path + Configuration::Instance()->get_vid_dirs_file());
	std::string dir, subdir;
	cout << dirs_path + Configuration::Instance()->get_vid_dirs_file() << endl;
	/*open subject dir*/
	while (std::getline(angles_dir, dir)) {
		string complete_dir = dirs_path + dir + "/data/";
		string complete_angles_file = complete_dir + angles_file;
		cout << "Reading files from: " << complete_angles_file << endl;
		vector<string> file_list;

		readXMLStringList(complete_angles_file, file_list);
		for (int x = 0; x < file_list.size(); x++) {
			Scalar scalar;
			int nAngles;

			cout << file_list[x] << endl;
			FileStorage fs2(complete_dir + file_list[x], FileStorage::READ);

			fs2[Configuration::Instance()->get_angles_size_variable_name()]
					>> nAngles;

			Mat angles_in_file, empty_mat;

			int count = 0;
			for (int i = 0; i < nAngles; ++i) {

				Mat temp;
				fs2[Configuration::Instance()->get_angles_variable_name()
						+ to_string(i)] >> temp;

				angles_in_file.push_back(temp);

				/*count++;

				 if (count == 40) {
				 count = 0;
				 Mat row_mean;
				 //cout << angles_in_file.rows << endl;
				 reduce(angles_in_file, row_mean, 0, CV_REDUCE_AVG);
				 angles.push_back(row_mean);
				 angles_in_file = empty_mat;

				 }*/

			}
			get_gait_cycles(angles_in_file, angles, complete_angles_file);
			/*if (angles_in_file.rows > 0) {
			 Mat row_mean;
			 reduce(angles_in_file, row_mean, 0, CV_REDUCE_AVG);
			 angles.push_back(row_mean);

			 }*/
			fs2.release();

		}

	}
	cout << angles.rows << endl;
}
void read_angles_predict(string dirs_path, string angles_file,
		cv::Ptr<cv::ml::SVM>& the_svn) {

	std::ifstream angles_dir(
			dirs_path + Configuration::Instance()->get_vid_dirs_file());
	std::string dir, subdir;
	int gcounter = 0;
	int bcounter = 0;

	/*open subject dir*/
	while (std::getline(angles_dir, dir)) {
		string complete_dir = dirs_path + dir + "/data/";
		string complete_angles_file = complete_dir + angles_file;
		cout << "Reading files from: " << complete_angles_file << endl;
		vector<string> file_list;

		readXMLStringList(complete_angles_file, file_list);

		for (int x = 0; x < file_list.size(); x++) {
			Scalar scalar;
			int nAngles;

			cout << file_list[x];

			FileStorage fs2(complete_dir + file_list[x], FileStorage::READ);

			fs2[Configuration::Instance()->get_angles_size_variable_name()]
					>> nAngles;
			Mat angles_in_file;
			int good = 0, bad = 0;
			for (int i = 0; i < nAngles; i++) {

				Mat temp;
				fs2[Configuration::Instance()->get_angles_variable_name()
						+ to_string(i)] >> temp;

				angles_in_file.push_back(temp);
				//int pred = the_svn->predict(temp);
				//if (pred > 1)
				//	good++;
				//else
				//	bad++;

			}

			Mat hist;

			string FileName = complete_angles_file;
			FileName.erase(FileName.size() - 4, FileName.size());
			string file = file_list[x];
			file.erase(file.size() - 4, file.size());
			FileName = FileName + "_" + file + "_hist.jpg";
			//cout << FileName << endl;
			calculate_histogram(FileName, angles_in_file, hist);
			//angles.push_back(hist);

			float response = the_svn->predict(hist);
			int pred = response;
			cout << "response: " << response << endl;

			if (pred >= 1) {
				cout << " Good :)" << endl;
				gcounter++;
			} else {
				cout << " Bad :(" << endl;
				bcounter++;
			}
			fs2.release();
		}

	}

	cout << "G counter: " << gcounter << endl;
	cout << "B counter: " << bcounter << endl;

}

void read_angles_predict_low_pass(string dirs_path, string angles_file,
		cv::Ptr<cv::ml::SVM>& the_svn, vector<pair<int, float>>& roi,
		int label) {

	std::ifstream angles_dir(
			dirs_path + Configuration::Instance()->get_vid_dirs_file());
	std::string dir, subdir;
	int gcounter = 0;
	int bcounter = 0;

	/*open subject dir*/
	while (std::getline(angles_dir, dir)) {
		string complete_dir = dirs_path + dir + "/data/";
		string complete_angles_file = complete_dir + angles_file;
		cout << "Reading files from: " << complete_angles_file << endl;
		vector<string> file_list;

		readXMLStringList(complete_angles_file, file_list);

		for (int x = 0; x < file_list.size(); x++) {
			Scalar scalar;
			int nAngles;

			cout << file_list[x];

			FileStorage fs2(complete_dir + file_list[x], FileStorage::READ);

			fs2[Configuration::Instance()->get_angles_size_variable_name()]
					>> nAngles;
			Mat angles_in_file;
			Mat empry_mat;
			int good = 0, bad = 0;

			int count = 0;
			for (int i = 0; i < nAngles; ++i) {

				Mat temp;
				fs2[Configuration::Instance()->get_angles_variable_name()
						+ to_string(i)] >> temp;

				angles_in_file.push_back(temp);
				/*count++;
				 if (count == 40) {
				 count = 0;
				 Mat row_mean;
				 //cout << angles_in_file.size() << endl;
				 reduce(angles_in_file, row_mean, 0, CV_REDUCE_AVG);
				 angles_in_file = empry_mat;

				 int pred = the_svn->predict(row_mean);
				 if (pred >= 1.0)
				 good++;
				 else
				 bad++;
				 }*/

			}

			Mat result;
			get_gait_cycles(angles_in_file, result, complete_angles_file);
			for (int x = 0; x < result.rows; x++) {
				Mat cycle = result.row(x);
				float response = the_svn->predict(cycle);
				int pred = response;

				if (pred >= 1.0)
					good++;
				else
					bad++;
			}

			/*if (angles_in_file.rows > 0) {
			 Mat row_mean;
			 //cout << angles_in_file.size() << endl;
			 reduce(angles_in_file, row_mean, 0, CV_REDUCE_AVG);
			 angles_in_file = empry_mat;

			 int pred = the_svn->predict(row_mean);
			 if (pred >= 1.0)
			 good++;
			 else
			 bad++;
			 }*/

			if (good >= bad) {
				float score = (float) good / (float) (bad + good);
				cout << " Normal (G, B) = (" << good << "," << bad << ")"
						<< " score: " << score << endl;
				roi.push_back(make_pair(label, score));
				gcounter++;
			} else {
				float score = (float) bad / (float) (bad + good);
				cout << " Abnormal (G, B) = (" << good << "," << bad << ")"
						<< " score: " << 1 - score << endl;
				bcounter++;
				roi.push_back(make_pair(label, 1 - score));
			}

			fs2.release();
		}

	}

	cout << "G counter: " << gcounter << endl;
	cout << "B counter: " << bcounter << endl;

}

void read_angles_predict_belly(string dirs_path, string angles_file,
		cv::Ptr<cv::ml::SVM>& the_svn) {

	std::ifstream angles_dir(
			dirs_path + Configuration::Instance()->get_vid_dirs_file());
	std::string dir, subdir;
	int gcounter = 0;
	int bcounter = 0;

	/*open subject dir*/
	while (std::getline(angles_dir, dir)) {
		string complete_dir = dirs_path + dir + "/data/";
		string complete_angles_file = complete_dir + angles_file;
		cout << "Reading files from: " << complete_angles_file << endl;
		vector<string> file_list;

		readXMLStringList(complete_angles_file, file_list);

		for (int x = 0; x < file_list.size(); x++) {
			Scalar scalar;
			int nAngles;

			cout << file_list[x];

			FileStorage fs2(complete_dir + file_list[x], FileStorage::READ);

			fs2[Configuration::Instance()->get_angles_size_variable_name()]
					>> nAngles;
			Mat angles_in_file;
			Mat empry_mat;
			int good = 0, bad = 0;

			int count = 0;

			for (int i = 0; i < nAngles; ++i) {

				Mat temp;
				float value[3];
				fs2[Configuration::Instance()->get_angles_variable_name()
						+ to_string(i)] >> temp;
				value[2] = temp.at<float>(0, 2);
				value[1] = temp.at<float>(0, 1);
				value[0] = temp.at<float>(0, 0);

				Mat val(1, 3, CV_32FC1, value);
				int pred = the_svn->predict(val);
				if (pred >= 1.0)
					good++;
				else
					bad++;

			}

			if (good >= bad) {
				cout << " Good :) (G, B) = (" << good << "," << bad << ")"
						<< endl;
				gcounter++;
			} else {
				cout << " Bad :( (G, B) = (" << good << "," << bad << ")"
						<< endl;
				bcounter++;
			}
			fs2.release();
		}

	}

	cout << "G counter: " << gcounter << endl;
	cout << "B counter: " << bcounter << endl;

}
void train_svn(cv::Ptr<cv::ml::SVM>& the_svn) {

	Mat good_angles, bad_angles;
	read_angles_low_pass(Configuration::Instance()->get_svn_train_dir(),
			Configuration::Instance()->get_good_angles_file(), good_angles);
	read_angles_low_pass(Configuration::Instance()->get_svn_train_dir(),
			Configuration::Instance()->get_bad_angles_file(), bad_angles);
	cout << "done done " << endl;
//Code based on opencv

//create labels matrix
	int lables_size = good_angles.rows + bad_angles.rows;
	int labels[lables_size];
	for (int i = 0; i < good_angles.rows; i++) {
		labels[i] = 1;
	}
	for (int i = good_angles.rows; i < lables_size; i++) {
		labels[i] = -1;
	}
	cv::Mat labels_mat(lables_size, 1, CV_32S, labels);

//training svm to based on walking or running videos
	cv::Mat data = good_angles;
	data.push_back(bad_angles);
	data.convertTo(data, CV_32FC1);
	cout << data.size() << endl;

	the_svn->train(data, cv::ml::ROW_SAMPLE, labels_mat);

	cout << "Training done:  " << endl;

}

void create_csv(vector<pair<int, float>> roi) {
	ofstream myfile;
	myfile.open("roi.csv");
	for (int i = 0; i < roi.size(); i++) {

		myfile << roi[i].first << "," << roi[i].second << "\n";

	}
	myfile.close();
}

void predict_svn(cv::Ptr<cv::ml::SVM>& the_svn) {
	vector<pair<int, float>> roi;

	read_angles_predict_low_pass(
			Configuration::Instance()->get_svn_predict_dir(),
			Configuration::Instance()->get_good_angles_file(), the_svn, roi, 1);
	read_angles_predict_low_pass(
			Configuration::Instance()->get_svn_predict_dir(),
			Configuration::Instance()->get_bad_angles_file(), the_svn, roi, -1);

	create_csv(roi);

}

int main() {

	std::ifstream results_file("./data/classification_results.txt");
	string line(" ") ;
	for(int x = 0; x < 100; x++)
	{
		class_results.push_back(line);
	}

	init();

	SACamera camera_left(Configuration::Instance()->get_left_camera_address(),
			Configuration::Instance()->get_camera_port()), camera_right(
			Configuration::Instance()->get_right_camera_address(),
			Configuration::Instance()->get_camera_port());
	SAMultiCameraHandler handler(camera_left, camera_right);

	cv::Ptr<cv::ml::SVM> the_svn;
	the_svn = cv::ml::SVM::create();
	the_svn->setType(cv::ml::SVM::C_SVC);
	the_svn->setKernel(cv::ml::SVM::LINEAR);
	//the_svn->setDegree(7);

//main menu
	bool go = true;
	while (go) {
		cout << "Menu: " << endl;
		cout << "1.- Live stream " << endl;
		cout << "2.- Take calibration images (low resolution)" << endl;
		cout << "3.- Stereo match" << endl;
		cout << "4.- Detect markers" << endl;
		cout << "5.- Stereo + marker detection + 3d model" << endl;
		cout << "6.- Stereo + marker detection + 3d model (Videos)" << endl;
		cout << "7.- Reproduce videos" << endl;
		cout << "8.- Train svn" << endl;
		cout << "9.- Predict svn" << endl;
		cout << "10.- Exit" << endl;
		int op;
		cin >> op;
		switch (op) {
		case 1:
			get_live_streams(camera_left, camera_right, handler);
			break;
		case 2:
			take_calibration_images(camera_left, camera_right, handler);
			break;
		case 3:
			stereo_match();
			break;
		case 4:
			detect_markers(camera_left, camera_right, handler);
			break;
		case 5:
			stereo_and_marker_detection(camera_left, camera_right, handler);
			break;
			/*case 6:
			 stereo_and_marker_detection_vidcap();
			 break;*/
		case 7:
			reproduce_videos();
			break;
		case 8:
			train_svn(the_svn);
			break;
		case 9:
			predict_svn(the_svn);
			break;
		case 10:
			go = false;
			break;

		}

	};

	Configuration::Delete();
	CameraParameters::Delete();
	return 0;
}

