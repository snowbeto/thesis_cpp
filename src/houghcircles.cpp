#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>

using namespace cv;
using namespace std;

static void help() {
	cout
			<< "\nThis program demonstrates circle finding with the Hough transform.\n"
					"Usage:\n"
					"./houghcircles <image_name>, Default is ../data/board.jpg\n"
			<< endl;
}

int main(int argc, char** argv) {
	VideoCapture cap(1); // open the default camera
	if (!cap.isOpened())  // check if we succeeded
		return -1;

	Mat img, cimg, hsv_hist;
	while (true)

	{
		cap >> img;

		//GaussianBlur(img, img, Size(9, 9), 2, 2);
		cv::cvtColor(img, hsv_hist, cv::COLOR_BGR2HSV);
		cv::inRange(hsv_hist, cv::Scalar(160, 100, 100),
				cv::Scalar(179, 255, 255), cimg);
		//cvtColor(img, img, COLOR_BGRA2GRAY);
		cv::dilate(cimg, cimg,
				getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
		cv::erode(cimg, cimg, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));

		//morphological closing (removes small holes from the foreground)
		dilate(cimg, cimg, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
		erode(cimg, cimg, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));

		//Canny(cimg, cimg, 20, 20 * 3, 3);

		vector<Vec3f> circles;
		HoughCircles(cimg, circles, HOUGH_GRADIENT, 1, cimg.rows / 5, 200, 30, 0,
				0 // change the last two parameters
				// (min_radius & max_radius) to detect larger circles
				);
		cout << "Number of detected circles " << circles.size() << endl;
		for (size_t i = 0; i < circles.size(); i++) {
			Vec3i c = circles[i];
			circle(cimg, Point(c[0], c[1]), c[2], Scalar(100, 50, 255), 3,
					LINE_AA);
			circle(cimg, Point(c[0], c[1]), 2, Scalar(100, 50, 25), 3, LINE_AA);
		}

		imshow("detected circles", cimg);
		imshow("Normal", img);
		int keyPressed = cv::waitKey(1);
		if (keyPressed == 1048603)
			break;

	}

	return 0;
}
