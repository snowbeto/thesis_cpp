#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

using namespace cv;
using namespace std;

/*
 * ./stereo_calib.o -w=8  -h=6 -s=7.2 stereo_calib.xml
 * */
static int print_help() {
	cout
			<< "This promgrams receives a set of videos and outputs a list of images\n"
			<< endl;
	cout
			<< "Usage:\n ./camera_frame_separator  <video list XML/YML file default=videos_list.xml>\n"
			<< endl;
	return 0;
}

void create_xml(vector<string> &left_images_list,
		vector<string> &right_images_list) {
	FILE* fp = fopen("./stereo_calib.xml", "wt");
	fprintf(fp, "<?xml version=\"1.0\"?>\n");
	fprintf(fp, "<opencv_storage>\n");
	fprintf(fp, "<imagelist>\n");

	for (int y = 0; y < left_images_list.size(); y++) {
		fprintf(fp, "\"%s\"\n", left_images_list[y].c_str());
		fprintf(fp, "\"%s\"\n", right_images_list[y].c_str());
	}
	fprintf(fp, "</imagelist>\n");
	fprintf(fp, "</opencv_storage>\n");
	fclose(fp);
}
static void separate_frames(const vector<string>& video_list) {
	if (video_list.size() % 2 != 0) {
		cout
				<< "Error: the video list contains odd (non-even) number of elements\n";
		return;
	}

	// ARRAY AND VECTOR STORAGE:
	vector<string> left_images_list, right_images_list;
	vector<Mat> left_images, right_images;
	Size imageSize;

	int i, j, k, num_videos = (int) video_list.size() / 2;
	VideoCapture vidleft, vidright;

	for (i = 0; i < num_videos; i++) {

		const string& left_video = video_list[i * 2];
		const string& right_video = video_list[i * 2 + 1];

		vidleft.open(left_video);
		vidright.open(right_video);

		Mat left_frame, right_frame;

		vidleft >> left_frame;
		vidright >> right_frame;

		string helper_string;
		string image_name("img");
		string image_extension(".jpg");
		if (i < 10)
			helper_string = "0";
		else
			helper_string = "";

		string left_image_name(
				"left" + image_name + helper_string + to_string(i)
						+ image_extension), right_image_name(
				"right" + image_name + helper_string + to_string(i)
						+ image_extension);

		cout << left_image_name << endl;
		cout << right_image_name << endl;

		imwrite(left_image_name, left_frame);
		imwrite(right_image_name, right_frame);

		left_images_list.push_back(left_image_name);
		right_images_list.push_back(right_image_name);

	}

	create_xml(left_images_list, right_images_list);
}

static bool readStringList(const string& filename, vector<string>& l) {
	l.resize(0);
	FileStorage fs(filename, FileStorage::READ);
	if (!fs.isOpened())
		return false;
	FileNode n = fs.getFirstTopLevelNode();
	if (n.type() != FileNode::SEQ)
		return false;
	FileNodeIterator it = n.begin(), it_end = n.end();
	for (; it != it_end; ++it)
		l.push_back((string) *it);
	return true;
}

int main(int argc, char** argv) {

	string listfilename;

	cv::CommandLineParser parser(argc, argv,
			"{@input|videos_list.xml|}{help||}");
	if (parser.has("help"))
		return print_help();

	listfilename = parser.get<string>("@input");

	if (!parser.check()) {
		parser.printErrors();
		return 1;
	}
	vector<string> video_list;
	bool ok = readStringList(listfilename, video_list);
	if (!ok || video_list.empty()) {
		cout << "can not open " << listfilename
				<< " or the string list is empty" << endl;
		return print_help();
	}

	separate_frames(video_list);
	return 0;
}
